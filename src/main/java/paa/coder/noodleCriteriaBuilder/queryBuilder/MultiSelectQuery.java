package paa.coder.noodleCriteriaBuilder.queryBuilder;

import org.hibernate.query.Query;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.exceptions.SelectionIsEmpty;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleQuery;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.OrderBuilder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SelectStore;
import paa.coder.noodleCriteriaBuilder.queryBuilder.specifications.NoodleSpecificationBuilder;

import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MultiSelectQuery<T> extends NoodleAbstractQuery.Select implements NoodleQuery<Tuple,NoodleResult> {

    protected final Class<T> from;
    protected final SelectStore.Multiply select;
    protected final OrderBuilder orderBuilder;

    public MultiSelectQuery(Class<T> from, List<NoodleSpecificationBuilder> where, NoodleFactory noodleFactory,OrderBuilder orderBuilder){
        super(noodleFactory);
        this.select = new SelectStore.Multiply(noodleFactory);
        this.orderBuilder = orderBuilder;
        this.where = where;
        this.from = from;
    }

    public MultiSelectQuery<T> where(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> fPred){
        super.where(fPred);
        return this;
    }

    public MultiSelectQuery<T> order(Consumer<OrderBuilder> fPred){
        fPred.accept(orderBuilder);
        return this;
    }

    public MultiSelectQuery<T> select(Consumer<SelectStore.Multiply> action){
        action.accept(select);
        return this;
    }

    public MultiSelectQuery<T> having(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> fPred){
        super.having(fPred);
        return this;
    }

    @Override
    public MultiSelectQuery<T> itDistinct(Boolean d){
        super.itDistinct(d);
        return this;
    }

    public MultiSelectQuery<T> group(Consumer<SelectStore> action){
        return group(action,false);
    }

    public MultiSelectQuery<T> group(Consumer<SelectStore> action,Boolean addToSelect){
        super.group(action);
        if(addToSelect){
            action.accept(select);
        }
        return this;
    }

    public CriteriaQuery<Tuple> criteriaQuery(String alias){
        CriteriaQuery<Tuple> criteriaQuery = noodleFactory.getSession().getCriteriaBuilder().createTupleQuery();
        Root<T> root = criteriaQuery.from(from);
        root.alias(alias);
        build(root, criteriaQuery, noodleFactory.getSession().getCriteriaBuilder());
        return criteriaQuery;
    }

    public CriteriaQuery<Tuple> criteriaQuery(){
        return criteriaQuery(null);
    }

    public Query<Tuple> query(){
        return noodleFactory.getSession().createQuery(criteriaQuery());
    }

    public Stream<NoodleResult> stream(Integer count, Integer offset){
        Query<Tuple> query = query();
        Optional.ofNullable(count).ifPresent(i->{
            query.setMaxResults(i);
            Optional.ofNullable(offset).ifPresent(query::setFirstResult);
        });
        return query.getResultStream().map(tuple -> {
            NoodleResult m = new NoodleResult();
            tuple.getElements().forEach(e -> m.put(e.getAlias(), tuple.get(e)));
            return m;
        });
    }

    public <X> Stream<X> projection(Class<X> tClass){
        return projection(tClass, null);
    }

    public <X> Stream<X> projection(Class<X> tClass, Integer count){
        return projection(tClass,count,null);
    }

    public <X> Stream<X> projection(Class<X> tClass, Integer count,Integer offset){
        return stream(count,offset).map(i -> noodleFactory.getObjectMapper().convertValue(i, tClass));
    }

    protected void build(Root<?> root, CriteriaQuery<Tuple> criteriaQuery, CriteriaBuilder criteriaBuilder){
        List<Expression<?>> selections = Optional
                .ofNullable(select.apply(pathFinder(root), criteriaQuery, criteriaBuilder))
                .filter(i -> ! i.isEmpty())
                .or(() -> Optional.of(getGroupBy().apply(pathFinder(root), criteriaQuery, criteriaBuilder)))
                .filter(i -> ! i.isEmpty())
                .or(() -> {
                    this.select.select(from);
                    return Optional.of(select.apply(pathFinder(root), criteriaQuery, criteriaBuilder));
                })
                .filter(i -> ! i.isEmpty())
                .orElseThrow(() -> new SelectionIsEmpty("not set find props"));


        criteriaQuery.multiselect(selections.stream().map(_i -> (Selection<?>) _i).collect(Collectors.toList()));
        criteriaQuery.orderBy(orderBuilder.apply(pathFinder(root), criteriaQuery, criteriaBuilder));
        super.build(root, criteriaQuery, criteriaBuilder);
    }

}
