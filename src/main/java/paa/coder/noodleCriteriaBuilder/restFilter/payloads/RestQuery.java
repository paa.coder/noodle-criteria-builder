package paa.coder.noodleCriteriaBuilder.restFilter.payloads;

import lombok.Data;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.queryBuilder.specifications.NoodleSpecificationBuilder;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

@Data
public class RestQuery {
    private List<RestCriteria> criteria = new ArrayList<>();
    private Predicate.BooleanOperator operator = Predicate.BooleanOperator.AND;


    public NoodleSpecificationBuilder build(NoodlePredicate.Builder builder, Function<String,Boolean> fieldValidator,
                                            Function<RestCriteria,Optional<BiFunction<RestCriteria,NoodlePredicate.Builder,NoodleSpecificationBuilder>>> spy){
        NoodleSpecificationBuilder nsb = builder.empty();

        for(RestCriteria c : criteria){
            if(c.getField() != null && fieldValidator.apply(c.getField())){
                Optional<BiFunction<RestCriteria,NoodlePredicate.Builder,NoodleSpecificationBuilder>> uFunc = spy.apply(c);
                if(uFunc!=null){
                    if(uFunc.isEmpty()){
                        continue;
                    }
                    if(operator==Predicate.BooleanOperator.AND){
                        nsb.and(b->uFunc.get().apply(c,b));
                    }else{
                        nsb.or(b->uFunc.get().apply(c,b));
                    }
                }else{
                    nsb = nsb.update(c.getField(), c.getOperator(), c.getValue(), operator);
                }
            }
            if(c.getQuery() != null){
                nsb = nsb.update(b -> c.getQuery().build(b, fieldValidator, spy), operator);
            }

        }
        return nsb;
    }

    public static class Builder {
        private RestQuery query;

        public RestQuery getQuery(){
            return query;
        }

        public Builder(RestQuery query){
            this.query = query;
        }

        private void add(RestCriteria sc, Predicate.BooleanOperator operator){
            if(query.getOperator().equals(operator)){
                query.getCriteria().add(sc);
            }else if(query.getCriteria().isEmpty()){
                query.getCriteria().add(sc);
                query.setOperator(operator);
            }else{
                RestQuery query = new RestQuery();
                query.setOperator(operator);
                query.getCriteria().add(new RestCriteria(this.query));
                query.getCriteria().add(sc);
                this.query = query;
            }
        }

        public Builder and(String field, String operator, Object value){
            return and(generate(field, operator, value));

        }

        public Builder and(RestCriteria searchCriteria){
            add(searchCriteria, Predicate.BooleanOperator.AND);
            return this;
        }

        public Builder and(Function<Builder,Builder> builder){
            return generate(builder, Predicate.BooleanOperator.AND);
        }

        public Builder and(String field, Object value){
            return and(field, "=", value);
        }

        public Builder or(String field, String operator, Object value){
            return or(generate(field, operator, value));
        }

        public Builder or(RestCriteria searchCriteria){
            add(searchCriteria, Predicate.BooleanOperator.OR);
            return this;
        }

        public Builder or(String field, Object value){
            return or(field, "=", value);
        }

        public Builder or(Function<Builder,Builder> builder){
            return generate(builder, Predicate.BooleanOperator.OR);
        }

        private Builder generate(Function<Builder,Builder> builder, Predicate.BooleanOperator operator){
            Optional
                    .of(new Builder(new RestQuery()))
                    .map(builder)
                    .map(Builder::getQuery)
                    .filter(i -> ! i.getCriteria().isEmpty())
                    .ifPresent(i -> add(new RestCriteria(i), operator));
            return this;
        }

        private RestCriteria generate(String field, String operator, Object value){
            return new RestCriteria(field, operator, value);
        }
    }
}
