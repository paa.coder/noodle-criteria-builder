package paa.coder.noodleCriteriaBuilder.interfaces;

import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaQuery;
import java.util.stream.Stream;

public interface NoodleQuery<T, V> {
    CriteriaQuery<T> criteriaQuery(String alias);

    CriteriaQuery<T> criteriaQuery();

    default Stream<V> stream() {
        return stream(null);
    }

    default Stream<V> stream(Integer count){
        return stream(count, null);
    }

    Stream<V> stream(Integer count, Integer offset);

    Query<T> query();
}
