package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import paa.coder.noodleCriteriaBuilder.exceptions.ComparisonOperatorsNotSupport;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;

import javax.persistence.criteria.*;
import java.util.function.Supplier;

public abstract class SubquerySpecification<T> extends NoodleSpecification{

    private final SubqueryBuilder<T> queryBuilder;

    public SubquerySpecification(SubqueryBuilder<T> queryBuilder,String operator, Boolean isNot){
        super(operator, isNot);
        this.queryBuilder = queryBuilder;
    }

    protected Predicate toStringPredicate(Expression<String> path,CommonAbstractCriteria criteriaQuery,PathFinder pathFinder,CriteriaBuilder b){
        return toStringExpression(path, (Expression<String>) createQuery(path, criteriaQuery, pathFinder), b);
    }

    protected  <X extends Comparable<X>> Predicate toComparablePredicate(Expression<X> path,CommonAbstractCriteria criteriaQuery,PathFinder pathFinder,CriteriaBuilder b){
        return toComparableExpression(path, createQuery(path, criteriaQuery, pathFinder), b);
    }
    protected <X> Predicate toEqualsPredicate(Expression<X> path,CommonAbstractCriteria criteriaQuery,PathFinder pathFinder,CriteriaBuilder b) {
        return toEqualsExpression(path,createQuery(path, criteriaQuery, pathFinder),b);
    }

    protected <X> Predicate toPredicateInList(Expression<? extends X> path, Subquery<? extends X> value, CriteriaBuilder b){
        if(value==null){
            return b.isNull(path);
        }
        Supplier<Predicate> in = () -> path.in(value);
        switch(operator.toUpperCase().trim()){
            case "IN":{
                return in.get();
            }
            case "NOT IN":{
                return in.get().not();
            }
        }
        throw new ComparisonOperatorsNotSupport("operator for collection must be in ['IN' ,'NOT IN']");
    }

    protected <X> Subquery<? extends X> createQuery(Expression<X> path,CommonAbstractCriteria criteriaQuery,PathFinder pathFinder){
        Class<? extends X> pathType = path.getJavaType();
        return new SubqueryBuilder.NoodleSubquery<>(queryBuilder,pathFinder,criteriaQuery.subquery(pathType)).build();
    }

    protected abstract Expression<?> getExpression(PathFinder pathFinder, CommonAbstractCriteria criteriaQuery, CriteriaBuilder criteriaBuilder);

    @Override
    public Predicate apply(PathFinder pathFinder, CommonAbstractCriteria criteriaQuery, CriteriaBuilder criteriaBuilder){
        Expression<?> path = getExpression(pathFinder,criteriaQuery,criteriaBuilder);
        Predicate predicate;
        if(isIterable()){
            predicate = toPredicateInList(path, createQuery(path,criteriaQuery,pathFinder), criteriaBuilder);
        }else{
            if(String.class.isAssignableFrom(path.getJavaType())){
                predicate = toStringPredicate((Expression<String>) path,criteriaQuery,pathFinder, criteriaBuilder);
            }else if(Comparable.class.isAssignableFrom(path.getJavaType())){
                predicate = toComparablePredicate((Expression<Comparable>)path,criteriaQuery,pathFinder, criteriaBuilder);
            }else{
                predicate = toEqualsPredicate(path,criteriaQuery,pathFinder, criteriaBuilder);
            }
        }


        return isNot ? predicate.not() : predicate;
    }
}
