package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataSet(cleanBefore = true)
class LiteralSubquerySpecificationTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toEquals(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereSubquery(b->b.from(Sample.class,"s").select(s->s.max("s.id")),1));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where 1=(select max(sample1_.id) from sample sample1_)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);
    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toNotEquals(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereSubquery(b->b.from(Sample.class,"s").select(s->s.max("s.id")),"<>",1));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where 1<>(select max(sample1_.id) from sample sample1_)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);

        MultiSelectQuery<Book> where1 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereSubquery(b->b.from(Sample.class,"s").select(s->s.max("s.id")),"!=",1));
        assertThat(SQLExtractor.from(where1.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where 1<>(select max(sample1_.id) from sample sample1_)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);


    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toComparing(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereSubquery(b->b.from(Sample.class,"s").select(s->s.max("s.id")),"<",1));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where 1<(select max(sample1_.id) from sample sample1_)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);

        MultiSelectQuery<Book> where2 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereSubquery(b->b.from(Sample.class,"s").select(s->s.max("s.id")),"<=",1));
        assertThat(SQLExtractor.from(where2.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where 1<=(select max(sample1_.id) from sample sample1_)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where2::stream);

        MultiSelectQuery<Book> where3 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereSubquery(b->b.from(Sample.class,"s").select(s->s.max("s.id")),">=",1));
        assertThat(SQLExtractor.from(where3.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where 1>=(select max(sample1_.id) from sample sample1_)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where3::stream);

        MultiSelectQuery<Book> where4 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereSubquery(b->b.from(Sample.class,"s").select(s->s.max("s.id")),">",1));
        assertThat(SQLExtractor.from(where4.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where 1>(select max(sample1_.id) from sample sample1_)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where4::stream);

    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toIn(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereSubquery(b->b.from(Sample.class,"s").select(s->s.select("s.id")), "in",1));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where 1 in (select sample1_.id from sample sample1_)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);

    }

}