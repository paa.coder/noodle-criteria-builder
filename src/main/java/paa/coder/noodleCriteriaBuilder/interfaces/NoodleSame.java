package paa.coder.noodleCriteriaBuilder.interfaces;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface NoodleSame {
    String[] except() default {};
    String[] only() default {};
}
