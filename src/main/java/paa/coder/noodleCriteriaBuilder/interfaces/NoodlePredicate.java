package paa.coder.noodleCriteriaBuilder.interfaces;


import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SelectStore;
import paa.coder.noodleCriteriaBuilder.queryBuilder.specifications.NoodleSpecificationBuilder;

import javax.persistence.criteria.Predicate;
import java.util.function.Consumer;
import java.util.function.Function;

public interface NoodlePredicate extends NoodleSelection<Predicate>{

    interface Builder{

        default NoodleSpecificationBuilder where(Consumer<SelectStore.Single> consumer, Object value){return where(consumer, "=", value);}
        NoodleSpecificationBuilder where(Consumer<SelectStore.Single> consumer, String operator, Object value);
        NoodleSpecificationBuilder whereNot(Consumer<SelectStore.Single> consumer, String operator, Object value);

        default NoodleSpecificationBuilder where(String field, Object value){return where(field, "=", value);}
        NoodleSpecificationBuilder where(String field, String operator, Object value);
        NoodleSpecificationBuilder whereNot(String field, String operator, Object value);

        NoodleSpecificationBuilder where(Function<Builder,NoodleSpecificationBuilder> fPred);
        NoodleSpecificationBuilder whereNot(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> fPred);

        default NoodleSpecificationBuilder whereSubquery(String field, SubqueryBuilder.Initiator.Runner value){return whereSubquery(field, "=", value);}
        NoodleSpecificationBuilder whereSubquery(String field, String operator, SubqueryBuilder.Initiator.Runner value);
        NoodleSpecificationBuilder whereSubqueryNot(String field, String operator, SubqueryBuilder.Initiator.Runner value);

        default NoodleSpecificationBuilder whereSubquery(SubqueryBuilder.Initiator.Runner value,Object o){return whereSubquery(value, "=", o);}
        NoodleSpecificationBuilder whereSubquery(SubqueryBuilder.Initiator.Runner value,String operator,Object o);
        NoodleSpecificationBuilder whereSubqueryNot(SubqueryBuilder.Initiator.Runner value,String operator,Object o);

        default NoodleSpecificationBuilder same(Object value) {return same(value,null);}
        NoodleSpecificationBuilder same(Object value,String prefix);
        NoodleSpecificationBuilder notSame(Object value,String prefix);

        NoodleSpecificationBuilder empty();



    }

}
