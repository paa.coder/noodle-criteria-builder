package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleSame;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;

import javax.persistence.criteria.CommonAbstractCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.lang.reflect.Field;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

public class SameSpecification implements NoodlePredicate {

    private final NoodleSpecificationBuilder builder;
    private final Object object;
    private final Function<Field,String> fieldNameBuilder;


    public SameSpecification(Builder builder, Object object, String path){
        this.builder = builder.empty();
        this.object = object;
        this.fieldNameBuilder = f -> String.format("%s%s", Optional.ofNullable(path).map(p->p+".").orElse(""), f.getName());
    }

    @Override
    public Predicate apply(PathFinder var1, CommonAbstractCriteria var2, CriteriaBuilder var3){
        Set<String> except = Set.of(object.getClass().getAnnotation(NoodleSame.class).except());
        Set<String> only = Set.of(object.getClass().getAnnotation(NoodleSame.class).only());
        Stream
                .of(object.getClass().getDeclaredFields())
                .filter(f -> only.isEmpty() || only.contains(f.getName()))
                .filter(f -> ! except.contains(f.getName()))
                .forEach(this::addField);
        return builder.apply(var1, var2, var3);
    }

    private void addField(Field f){
        f.setAccessible(true);
        try{
            if(f.getType().isAnnotationPresent(NoodleSame.class)){
                Function<Field,String> fb = s -> String.format("%s.%s", fieldNameBuilder.apply(f), s.getName());
                Optional
                        .ofNullable(f.get(object))
                        .ifPresentOrElse(i -> builder.andSame(i, fieldNameBuilder.apply(f)), () -> builder.and(_b -> allNull(_b, f.getType(), fb)));
            }else{
                Optional
                        .ofNullable(f.get(object))
                        .ifPresentOrElse(i -> builder.and(fieldNameBuilder.apply(f), i), () -> builder.andIsNul(fieldNameBuilder.apply(f)));
            }
        }catch(IllegalAccessException e){
            throw new NoodleException(String.format("Noodle example build error, has not access to field: %s in class %s",
                                                    f.getName(),
                                                    object.getClass().getSimpleName()), e);
        }
    }

    private NoodleSpecificationBuilder allNull(NoodlePredicate.Builder b, Class<?> o, Function<Field,String> fnb){
        Set<String> except = Set.of(o.getAnnotation(NoodleSame.class).except());
        Set<String> only = Set.of(o.getAnnotation(NoodleSame.class).only());
        NoodleSpecificationBuilder nb = b.empty();
        Stream
                .of(o.getDeclaredFields())
                .filter(f -> only.isEmpty() || only.contains(f.getName()))
                .filter(f -> ! except.contains(f.getName()))
                .forEach(f -> {
                    if(f.getType().isAnnotationPresent(NoodleSame.class)){
                        nb.and(_b -> allNull(_b, f.getType(), s -> String.format("%s.%s", fnb.apply(f), s)));
                    }else{
                        nb.andIsNul(fnb.apply(f));
                    }
                });
        return nb;
    }
}
