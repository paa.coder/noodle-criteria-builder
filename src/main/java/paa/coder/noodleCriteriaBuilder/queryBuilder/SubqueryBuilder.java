package paa.coder.noodleCriteriaBuilder.queryBuilder;

import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SelectStore;
import paa.coder.noodleCriteriaBuilder.queryBuilder.specifications.NoodleSpecificationBuilder;

import javax.persistence.criteria.*;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class SubqueryBuilder<T> extends NoodleAbstractQuery.Select {


    protected final SelectStore.Single select;
    protected final Class<T> from;
    protected final String alias;

    public SubqueryBuilder(NoodleFactory noodleFactory, Class<T> from, String alias){
        super(noodleFactory);
        this.select = new SelectStore.Single();
        this.from = from;
        this.alias = alias;
    }

    public SubqueryBuilder(SubqueryBuilder<T> builder){
        super(builder.noodleFactory);
        this.select = builder.select;
        this.groupBy = builder.groupBy;
        this.where = builder.where;
        this.having = builder.having;
        this.from = builder.from;
        this.alias = builder.alias;
        this.distinct= builder.distinct;
    }

    @Override
    public SubqueryBuilder<T> where(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> fPred){
        super.where(fPred);
        return this;
    }

    public SubqueryBuilder<T> select(Consumer<SelectStore.Single> action){
        action.accept(select);
        return this;
    }

    @Override
    public SubqueryBuilder<T> having(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> fPred){
        super.having(fPred);
        return this;
    }

    @Override
    public SubqueryBuilder<T> group(Consumer<SelectStore> action){
        super.group(action);
        return this;
    }

    @Override
    public SubqueryBuilder<T> itDistinct(Boolean d){
        super.itDistinct(d);
        return this;
    }

    public static class Initiator {
        private final NoodleFactory noodleFactory;

        public Initiator(NoodleFactory noodleFactory){
            this.noodleFactory = noodleFactory;
        }

        public <T> SubqueryBuilder<T> from(Class<T> clazz, String alias){
            return new SubqueryBuilder<>(noodleFactory, clazz, alias);
        }

        public interface Runner extends Function<Initiator,SubqueryBuilder<?>> {

        }
    }

    public static class NoodleSubquery<T, W> extends SubqueryBuilder<W> {

        private final PathFinder parentPathFinder;
        private final Subquery<T> subquery;

        public NoodleSubquery(SubqueryBuilder<W> builder, PathFinder parentPathFinder, Subquery<T> subquery){
            super(builder);
            this.parentPathFinder = parentPathFinder;
            this.subquery = subquery;
            this.subquery.alias(alias);
        }

        public static <W> NoodleSubquery<?, W> sample(SubqueryBuilder<W> builder, PathFinder parentPathFinder,CommonAbstractCriteria criteriaQuery){
            return new NoodleSubquery<>(builder,parentPathFinder, criteriaQuery.subquery(builder.from));
        }
        @Override
        public Optional<PathFinder> parentFinder(){
            return Optional.of(parentPathFinder);
        }

        @Override
        public Optional<String> alias(){
            return Optional.of(alias);
        }

        public Subquery<T> build(){
            Root<W> root = subquery.from(from);
            build(root, subquery, noodleFactory.getSession().getCriteriaBuilder());
            return subquery;
        }

        protected void build(Root<?> root, Subquery<T> criteriaQuery, CriteriaBuilder criteriaBuilder){
            select.apply(pathFinder(root), criteriaQuery, criteriaBuilder).or(() -> Optional.of(root)).ifPresent(i -> criteriaQuery.select((Expression<T>) i));
            super.build(root, criteriaQuery, criteriaBuilder);
        }
    }
}
