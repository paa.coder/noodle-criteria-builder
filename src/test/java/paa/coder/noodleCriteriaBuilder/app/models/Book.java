package paa.coder.noodleCriteriaBuilder.app.models;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@Entity
@Table(name = "book")
public class Book {
    @Id
    private Integer id;
    private String name;

    private Info info = new Info();

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;

    @Column(name = "author_id",updatable = false,insertable = false)
    private Integer authorId;

    @Data
    @Embeddable
    public static class Info implements Serializable {
        private Instant date;
        private String comment;
        private Integer size;
    }


}
