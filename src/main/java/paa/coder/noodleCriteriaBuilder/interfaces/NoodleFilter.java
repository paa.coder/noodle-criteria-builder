package paa.coder.noodleCriteriaBuilder.interfaces;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface NoodleFilter {
    @AliasFor("value")
    String path() default "";
    @AliasFor("path")
    String value() default "";

    String criteriaFieldNamePolicy() default "";
}
