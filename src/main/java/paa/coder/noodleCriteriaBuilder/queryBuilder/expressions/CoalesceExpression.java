package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;
import paa.coder.noodleCriteriaBuilder.interfaces.CoalesceBuilder;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.interfaces.ThreeFunction;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;

import javax.persistence.criteria.CommonAbstractCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import java.util.Optional;
import java.util.function.Function;

public class CoalesceExpression implements NoodleExpression {


    private final ThreeFunction<PathFinder,CommonAbstractCriteria,CriteriaBuilder,Expression<?>> expresion;

    private CoalesceExpression(ThreeFunction<PathFinder,CommonAbstractCriteria,CriteriaBuilder,Expression<?>> expresion){
        this.expresion = expresion;
    }

    public ThreeFunction<PathFinder,CommonAbstractCriteria,CriteriaBuilder,Expression<?>> getExpresion(){
        return expresion;
    }

    private CoalesceExpression action(ThreeFunction<Expression<?>,Path<?>,CriteriaBuilder,Expression<?>> updater, String... fields){
        if(fields.length == 0){
            return this;
        }
        return new CoalesceExpression((root, query, criteriaBuilder) -> {
            Expression<?> expression = expresion.apply(root, query, criteriaBuilder);
            for(String field : fields){
                expression = updater.apply(expression, path(root, field), criteriaBuilder);
            }
            return expression;
        });
    }

    private CoalesceExpression actionLiteral(ThreeFunction<Expression<?>,Expression<?>,CriteriaBuilder,Expression<?>> updater,
                                             Function<CriteriaBuilder,Expression<?>> literalBuilder){

        return new CoalesceExpression((root, query, criteriaBuilder) -> updater.apply(expresion.apply(root, query, criteriaBuilder),
                                                                                      literalBuilder.apply(criteriaBuilder),
                                                                                      criteriaBuilder));
    }

    public CoalesceExpression or(String... fields){
        if(fields.length == 0){
            return this;
        }
        return action((oe, p, cb) -> cb.coalesce(oe, p), fields);
    }

    public CoalesceExpression orLiteral(Object o){
        if(o == null){
            return this;
        }
        return actionLiteral((oe, p, cb) -> cb.coalesce(oe, p), cr -> cr.literal(o));
    }

    public CoalesceExpression orNull(Class<?> clazz){
        return actionLiteral((oe, p, cb) -> cb.coalesce(oe, p), cb -> cb.nullLiteral(clazz));
    }

    public CoalesceExpression orMath(Function<ArithmeticExpression.Builder,ArithmeticExpression> uFunc){
        return new CoalesceExpression((root, query, criteriaBuilder) -> criteriaBuilder.coalesce(expresion.apply(root, query, criteriaBuilder),
                                                                                                 uFunc
                                                                                                         .apply(new ArithmeticExpression.Builder())
                                                                                                         .getExpresion()
                                                                                                         .apply(root, query, criteriaBuilder)));
    }

    public CoalesceExpression orConcat(Function<ConcatExpression,ConcatExpression> uFunc){
        return new CoalesceExpression((root, query, criteriaBuilder) -> criteriaBuilder.coalesce(expresion.apply(root, query, criteriaBuilder),
                                                                                                 uFunc
                                                                                                         .apply(new ConcatExpression())
                                                                                                         .getExpresion()
                                                                                                         .apply(root, query, criteriaBuilder)));
    }

    public CoalesceExpression orSubquery(SubqueryBuilder<?> subqueryBuilder, Class<?> clazz){
        return new CoalesceExpression((root, query, criteriaBuilder) -> {
            return criteriaBuilder.coalesce(expresion.apply(root, query, criteriaBuilder),
                                            new SubqueryBuilder.NoodleSubquery<>(subqueryBuilder, root, query.subquery(clazz)).build());
        });
    }

    static Path<?> path(PathFinder pathFinder, String fieldName){
        return pathFinder
                .apply(SampleColumn.build(fieldName).getName())
                .orElseThrow(() -> new NoodleException(String.format("Field %s not found", fieldName)));

    }

    @Override
    public Optional<Expression<?>> apply(PathFinder root, CommonAbstractCriteria query, CriteriaBuilder criteriaBuilder){
        return Optional.ofNullable(expresion).map(i -> i.apply(root, query, criteriaBuilder));
    }

    public static class Builder implements CoalesceBuilder {

        @Override
        public CoalesceExpression by(String field){
            return new CoalesceExpression((r, query, c) -> path(r, field));
        }

        public CoalesceExpression subquery(SubqueryBuilder<?> subqueryBuilder, Class<?> clazz){
            return new CoalesceExpression((root, query, criteriaBuilder) -> {
                return new SubqueryBuilder.NoodleSubquery<>(subqueryBuilder, root, query.subquery(clazz)).build();
            });
        }
    }

}
