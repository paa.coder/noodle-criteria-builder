package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import paa.coder.noodleCriteriaBuilder.interfaces.*;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderBuilder implements NoodleSelection<List<Order>> {

    private final LinkedList<NoodleSelection<Optional<Order>>> list = new LinkedList<>();

    public void add(Boolean asc, String... fields){
        Stream.of(fields).forEach(i -> add(asc,SampleColumn.build(i, null)));
    }


    private void add(Boolean asc, NoodleExpression i){
        NoodleSelection<Optional<Order>> ns = (p, c, cb) -> i.apply(p, c, cb).map(expression -> new Order() {
            private boolean ascending = asc;
            @Override
            public Order reverse(){
                this.ascending = ! this.ascending;
                return this;
            }
            @Override
            public boolean isAscending(){
                return this.ascending;
            }
            @Override
            public Expression<?> getExpression(){
                return expression;
            }
        });
        list.add(ns);

    }

    private void add(Boolean asc, List<NoodleExpression> i){
       i.forEach(ex->add(asc,ex));

    }

    public OrderBuilder add(Boolean asc, Function<ExpressionBuilder,Optional<NoodleExpression>> uFunc){
        return addExpressions(asc,eb->uFunc.apply(eb).stream().collect(Collectors.toList()));
    }

    public OrderBuilder addExpressions(Boolean asc, Function<ExpressionBuilder,List<NoodleExpression>> uFunc){
        add(asc,uFunc.apply(new ExpressionBuilder()));
        return this;
    }

    public OrderBuilder asc(Function<ExpressionBuilder,Optional<NoodleExpression>> uFunc){
        return add(true,uFunc);
    }

    public OrderBuilder desc(Function<ExpressionBuilder,Optional<NoodleExpression>> uFunc){
        return add(false,uFunc);
    }


    public OrderBuilder asc(String... fields){
        add(true, fields);
        return this;
    }

    public OrderBuilder desc(String... fields){
        add(false, fields);
        return this;
    }

    @Override
    public List<Order> apply(PathFinder pathFinder, CommonAbstractCriteria criteria, CriteriaBuilder cb){
        return list.stream().map(i -> i.apply(pathFinder, criteria, cb)).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
    }

    public static class ExpressionBuilder{
        protected final NoodleExpression.Builder builder = new NoodleExpression.Builder() {};
        private  List<NoodleExpression> complex = new ArrayList<>();

        public Optional<NoodleExpression> coalesce(Function<CoalesceBuilder,CoalesceExpression> userFunc){
            return builder.coalesce(null,userFunc).peek(complex::add).findFirst();
        }

        public Optional<NoodleExpression> custom(BiFunction<Function<String,Path<?>>,CriteriaBuilder,Expression<?>> userFunc){
            return builder.custom(null,userFunc).peek(complex::add).findFirst();
        }

        public Optional<NoodleExpression> math(Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            return builder.math(null,userFunc).peek(complex::add).findFirst();
        }

        public Optional<NoodleExpression> concat(Function<ConcatExpression,ConcatExpression> userFunc){
            return builder.concat(null, userFunc).peek(complex::add).findFirst();
        }

        public Optional<NoodleExpression> length(String field){
            return builder.length(field).peek(complex::add).findFirst();
        }

        public Optional<NoodleExpression> field(String field){
            return builder.field(field).peek(complex::add).findFirst();
        }

        public Optional<NoodleExpression> count(String fields){
            return builder.count(fields).findFirst();
        }
        public List<NoodleExpression> build(){
            final ArrayList<NoodleExpression> noodleExpressions = new ArrayList<>(complex);
            this.complex=new ArrayList<>();
            return noodleExpressions;
        }
    }
}
