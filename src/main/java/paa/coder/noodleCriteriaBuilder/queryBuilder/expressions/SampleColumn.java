package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import lombok.Data;
import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;

import javax.persistence.criteria.CommonAbstractCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

@Data
public class SampleColumn implements NoodleExpression {

    private final String name;
    private final String alias;

    private SampleColumn(String name){
        this(name, name);
    }

    private SampleColumn(String name, String alias){
        this.name = name;
        this.alias = alias;
    }

    public static SampleColumn build(String field){
        return build(field,null);
    }

    public static SampleColumn build(String field, String alias){
        if(field==null || field.isBlank()){
            throw new NoodleException("Field must not be empty");
        }

        Function<String,String> cleaning = (i) -> i.trim().replaceAll("\\s", "_");
        String[] fa = field.split(" [aA][sS] ", 2);
        SampleColumn f;
        if(fa.length == 2){
            f = new SampleColumn(cleaning.apply(fa[0]), cleaning.apply(fa[1]));
        }else{
            f = new SampleColumn(cleaning.apply(fa[0]));
        }
        return Optional
                .ofNullable(alias)
                .map(cleaning)
                .filter(Predicate.not(String::isBlank))
                .map(s -> new SampleColumn(String.join(".", s, f.getName()), f.getAlias()))
                .orElse(f);
    }

    @Override
    public Optional<Path<?>> apply(PathFinder root, CommonAbstractCriteria abstractQuery, CriteriaBuilder criteriaBuilder){
        return root.apply(getName()).map(i->{
            if(i.getAlias()==null || i.getAlias().equals(getName())){
                i.alias(getAlias());
            }
            return i;
        });
    }
}
