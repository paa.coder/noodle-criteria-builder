package paa.coder.noodleCriteriaBuilder.app.models;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "author2")
@EqualsAndHashCode(exclude = "books")
@ToString(exclude = "books")
public class Author2 {
    @Id
    private Integer id;
    private String name;

    @OneToMany(fetch = FetchType.EAGER,mappedBy = "author")
    private Set<Book2> books;
}
