package paa.coder.noodleCriteriaBuilder.interfaces;

@FunctionalInterface
public interface ThreeFunction<T, U, X, R> {
    R apply(T var1, U var2, X var3);
}