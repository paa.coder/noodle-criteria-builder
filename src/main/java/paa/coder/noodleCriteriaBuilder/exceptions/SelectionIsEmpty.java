package paa.coder.noodleCriteriaBuilder.exceptions;

public class SelectionIsEmpty extends NoodleException{

    public SelectionIsEmpty(){
    }

    public SelectionIsEmpty(String message){
        super(message);
    }

    public SelectionIsEmpty(String message, Throwable cause){
        super(message, cause);
    }

    public SelectionIsEmpty(Throwable cause){
        super(cause);
    }

    public SelectionIsEmpty(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
