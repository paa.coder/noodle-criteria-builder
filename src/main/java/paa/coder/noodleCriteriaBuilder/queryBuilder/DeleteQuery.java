package paa.coder.noodleCriteriaBuilder.queryBuilder;

import org.hibernate.query.Query;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.queryBuilder.specifications.NoodleSpecificationBuilder;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import java.util.function.Function;

public class DeleteQuery<T> extends NoodleAbstractQuery{

    protected final Class<T> from;

    public DeleteQuery(Class<T> from, NoodleFactory noodleFactory){
        super(noodleFactory);
        this.from = from;
    }

    @Override
    public DeleteQuery<T> where(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> fPred){
        super.where(fPred);
        return this;
    }

    public Integer delete(){
        find(from).forEach(i->noodleFactory.getSession().detach(i));
        return deleteForce();
    }

    public Query query(){
        CriteriaDelete<T> delete = noodleFactory.getSession().getCriteriaBuilder().createCriteriaDelete(from);
        Root<T> root = delete.from(from);
        getWherePredicates(root,delete,noodleFactory.getSession().getCriteriaBuilder()).ifPresent(delete::where);
        return noodleFactory.getSession().createQuery(delete);
    }

    public Integer deleteForce(){
        return query().executeUpdate();
    }
}
