package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import com.fasterxml.jackson.core.JsonProcessingException;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;
import paa.coder.noodleCriteriaBuilder.interfaces.ArithmeticBuilder;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

public class SetterBuilder {
    private final Map<SampleColumn,Function<Path<?>,NoodleExpression>> mapExpressions = new HashMap<>();
    private final NoodleFactory factory;
    private final NoodleExpression.Builder builder = new NoodleExpression.Builder() {
    };

    private void add(String field, Function<Path<?>,NoodleExpression> expression){
        mapExpressions.put(SampleColumn.build(field, null), expression);
    }

    public SetterBuilder(NoodleFactory factory){
        this.factory = factory;
    }

    public Map<SampleColumn,Function<Path<?>,NoodleExpression>> getMapExpressions(){
        return mapExpressions;
    }

    private NoodleExpression wrapper(NoodleExpression expression,Path<?> path){
        return (p,q,c)->expression.apply(p,q,c).map(_i->_i.as(path.getJavaType()));
    }

    public SetterBuilder custom(String field, BiFunction<Function<String,Path<?>>,CriteriaBuilder,Expression<?>> userFunc){
        builder.custom(null, userFunc).findFirst().ifPresent(i -> add(field, _f->wrapper(i,_f)));
        return this;
    }

    public SetterBuilder math(String field, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
        builder.math(null, userFunc).findFirst().ifPresent(i -> add(field, _f->wrapper(i,_f)));
        return this;
    }

    public SetterBuilder concat(String field, Function<ConcatExpression,ConcatExpression> userFunc){
        builder.concat(null, userFunc).findFirst().ifPresent(i -> add(field, _f->wrapper(i,_f)));
        return this;
    }

    public SetterBuilder subquery(String field, SubqueryBuilder.Initiator.Runner userFunc){
        NoodleExpression wrap = (pathFinder, query, criteriaBuilder) -> {
            Expression<?> expression = new SubqueryBuilder.NoodleSubquery<>(userFunc.apply(new SubqueryBuilder.Initiator(factory)),
                                                                            pathFinder,
                                                                            query.subquery(Object.class)).build();
            return Optional.of(expression);
        };
        add(field, _f->wrapper(wrap,_f));
        return this;
    }

    public SetterBuilder set(String field, Object o){
        final SampleColumn sc = SampleColumn.build(field, null);
        if(o==null){
            mapExpressions.put(sc, _f -> (pathFinder, query, criteriaBuilder) -> Optional.of(criteriaBuilder.nullLiteral(_f.getJavaType())));
            return this;
        }

        mapExpressions.put(sc, _f -> (pathFinder, query, criteriaBuilder) -> {
            try{
                return Optional.of(criteriaBuilder.literal(factory.getValue(_f.getJavaType(), o)));
            }catch(JsonProcessingException e){
                throw new NoodleException(String.format("can not set object of type %s to field %s with type %s",
                                                        o.getClass(),
                                                        field,
                                                        _f.getJavaType()));
            }
        });
        return this;
    }
}
