package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import lombok.Data;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Author;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleSame;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SampleQuery;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

@DataSet(cleanBefore = true)
class SameSpecificationTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/sampleSpecificationTest.yml", transactional = true)
    void same(){
        BookClone s = new BookClone();
        s.setName("Dune");
        s.setAuthorId(2);
        MultiSelectQuery<Book> sq = factory().select(Book.class).select(sel -> sel.select("id")).where(w -> w.same(s));
        assertThat(SQLExtractor.from(sq.query())).endsWith(
                "from book book0_ where book0_.name=? and (book0_.comment is null) and (book0_.size is null) and book0_.author_id=2");
        assertThat(sq.stream(1)).hasSize(1).first().hasFieldOrPropertyWithValue("id", 3);

        MultiSelectQuery<Book> sq2 = factory().select(Book.class).select(sel -> sel.select("id")).where(w -> w.notSame(s,null));
        assertThat(SQLExtractor.from(sq2.query())).endsWith(
                "from book book0_ where book0_.name<>? or book0_.comment is not null or book0_.size is not null or book0_.author_id<>2");


        BookClone.Info info = new BookClone.Info();
        info.setComment("The Man in the High Castle");
        info.setSize(10);

        MultiSelectQuery<Book> sq1 = factory().select(Book.class).select(sel -> sel.select("id")).where(w -> w.same(info,"info"));
        assertThat(SQLExtractor.from(sq1.query())).endsWith(
                "from book book0_ where book0_.comment=? and book0_.size=10");

        MultiSelectQuery<Book> sq3 = factory().select(Book.class).select(sel -> sel.select("id")).where(w -> w.notSame(info,"info"));
        assertThat(SQLExtractor.from(sq3.query())).endsWith(
                "from book book0_ where book0_.comment<>? or book0_.size<>10");

        s.setInfo(null);
        MultiSelectQuery<Book> sq4 = factory().select(Book.class).select(sel -> sel.select("id")).where(w -> w.same(s));
        assertThat(SQLExtractor.from(sq4.query())).endsWith(
                "from book book0_ where book0_.name=? and (book0_.comment is null) and (book0_.size is null) and book0_.author_id=2");



    }

    @Test
    @DataSet(value = "sample/sampleSpecificationTest.yml", transactional = true)
    void iLike(){

        SampleQuery<Book> sq5 = factory().select(Book.class).where(_s->_s.where("name", "ilike", "ТеСТ"));
        assertThat(SQLExtractor.from(sq5.query())).endsWith(
                "where lower(book0_.name) like ?");

        assertThat(sq5.query().getQueryString()).endsWith(
                "where lower(generatedAlias0.name) like :param0");

        assertThat(getParamValue("param0",sq5.query())).isEqualTo("тест");

    }

    @Test
    @DataSet(value = "sample/sampleSpecificationTest.yml", transactional = true)
    void andOr(){
        BookClone.Info info = new BookClone.Info();
        info.setComment("The Man in the High Castle");
        info.setSize(10);

        MultiSelectQuery<Book> sq1 = factory().select(Book.class).select(sel -> sel.select("id")).where(w -> w.where("authorId",2).andSame(info,"info"));
        assertThat(SQLExtractor.from(sq1.query())).endsWith(
                "from book book0_ where book0_.author_id=2 and book0_.comment=? and book0_.size=10");

        MultiSelectQuery<Book> sq2 = factory().select(Book.class).select(sel -> sel.select("id")).where(w -> w.where("authorId",2).orSame(info,"info"));
        assertThat(SQLExtractor.from(sq2.query())).endsWith(
                "from book book0_ where book0_.author_id=2 or book0_.comment=? and book0_.size=10");

    }

    @Data
    @NoodleSame(except = {"author", "id"})
    public static class BookClone {
        private Integer id;
        private String name;

        private Info info = new BookClone.Info();
        private Author author;
        private Integer authorId;

        @Data
        @NoodleSame(only = {"comment", "size"})
        public static class Info {
            private Instant date;
            private String comment;
            private Integer size;
        }


    }

}