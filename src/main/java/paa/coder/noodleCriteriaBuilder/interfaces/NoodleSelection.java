package paa.coder.noodleCriteriaBuilder.interfaces;

import javax.persistence.criteria.CommonAbstractCriteria;
import javax.persistence.criteria.CriteriaBuilder;

public interface NoodleSelection<T> extends ThreeFunction<PathFinder,CommonAbstractCriteria,CriteriaBuilder,T>{
}
