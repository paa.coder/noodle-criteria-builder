package paa.coder.noodleCriteriaBuilder.app.models;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "orders")
public class Order {
    @Id
    private Integer id;
    private String name;
}
