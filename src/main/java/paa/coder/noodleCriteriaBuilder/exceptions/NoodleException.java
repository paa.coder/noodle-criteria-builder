package paa.coder.noodleCriteriaBuilder.exceptions;

public class NoodleException extends RuntimeException{

    public NoodleException(){
    }

    public NoodleException(String message){
        super(message);
    }

    public NoodleException(String message, Throwable cause){
        super(message, cause);
    }

    public NoodleException(Throwable cause){
        super(cause);
    }

    public NoodleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public static class FieldNotFound extends IllegalArgumentException{
        public final Class<?> rootClass;
        private final String field;

        public FieldNotFound(String message, Throwable cause, Class<?> rootClass, String field){
            super(message, cause);
            this.rootClass = rootClass;
            this.field = field;
        }
    }

    public static class FieldBlocked extends IllegalArgumentException{
        public final String field;

        public FieldBlocked(String s,Throwable cause, String field){
            super(s,cause);
            this.field = field;
        }
    }
}
