package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Author;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataSet(cleanBefore = true)
class ExistSpecificationTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void apply(){
        MultiSelectQuery<Author> where = factory()
                .select(Author.class)
                .select(s -> s.select("id"))
                .where(w -> w.empty().andIsNotNul("name").andExists(b -> b.from(Book.class, "book")
                        .where(_w -> _w.where("book.authorId", "#id").and("book.name", "The Man in the High Castle"))));
        assertThat(SQLExtractor.from(where.query())).isEqualTo(
                "select author0_.id as col_0_0_ from author author0_ where (author0_.name is not null) and (exists (select book1_.id from book book1_ where book1_.author_id=author0_.id and book1_.name=?))");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);
    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void applyOrNot(){
        MultiSelectQuery<Author> where = factory()
                .select(Author.class)
                .select(s -> s.select("id"))
                .where(w -> w.empty().andIsNotNul("name").orNotExists(b -> b.from(Book.class, "book")
                        .where(_w -> _w.where("book.authorId", "#id").and("book.name", "The Man in the High Castle"))));
        assertThat(SQLExtractor.from(where.query())).isEqualTo(
                "select author0_.id as col_0_0_ from author author0_ where author0_.name is not null or  not (exists (select book1_.id from book book1_ where book1_.author_id=author0_.id and book1_.name=?))");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);
    }
}