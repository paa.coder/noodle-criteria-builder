package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(JUnitPlatform.class)
class SampleColumnTest {

    @Test
    public void buildingSampleTests(){

        assertThatThrownBy(() -> SampleColumn.build(null, null))
                .isInstanceOf(NoodleException.class)
                .hasFieldOrPropertyWithValue("message", "Field must not be empty");
        assertThatThrownBy(() -> SampleColumn.build("", null))
                .isInstanceOf(NoodleException.class)
                .hasFieldOrPropertyWithValue("message", "Field must not be empty");

        assertThat(SampleColumn.build(" value  as aliasValue", null))
                .isNotNull()
                .hasFieldOrPropertyWithValue("name", "value")
                .hasFieldOrPropertyWithValue("alias", "aliasValue");

        assertThat(SampleColumn.build("cost As  alias.cost ", null))
                .isNotNull()
                .hasFieldOrPropertyWithValue("name", "cost")
                .hasFieldOrPropertyWithValue("alias", "alias.cost");

        assertThat(SampleColumn.build("created aS created as sum", "test"))
                .isNotNull()
                .hasFieldOrPropertyWithValue("name", "test.created")
                .hasFieldOrPropertyWithValue("alias", "created_as_sum");

        assertThat(SampleColumn.build("asc as cask", "test"))
                .isNotNull()
                .hasFieldOrPropertyWithValue("name", "test.asc")
                .hasFieldOrPropertyWithValue("alias", "cask");

    }
}