package paa.coder.noodleCriteriaBuilder.exceptions;

public class ComparisonOperatorsNotSupport  extends NoodleException{

    public ComparisonOperatorsNotSupport(){
    }

    public ComparisonOperatorsNotSupport(String message){
        super(message);
    }

    public ComparisonOperatorsNotSupport(String message, Throwable cause){
        super(message, cause);
    }

    public ComparisonOperatorsNotSupport(Throwable cause){
        super(cause);
    }

    public ComparisonOperatorsNotSupport(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public static class List extends ComparisonOperatorsNotSupport{
        public List(String message, Throwable cause){
            super(message, cause);
        }
    }

    public static class Value extends ComparisonOperatorsNotSupport{
        public Value(String message, Throwable cause){
            super(message, cause);
        }
    }
}
