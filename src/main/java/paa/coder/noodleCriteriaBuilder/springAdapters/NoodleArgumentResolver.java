package paa.coder.noodleCriteriaBuilder.springAdapters;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleFilter;
import paa.coder.noodleCriteriaBuilder.restFilter.NoodleRestFilter;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestFilter;
import paa.coder.noodleCriteriaBuilder.springAdapters.converters.RestFieldConverter;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class NoodleArgumentResolver implements HandlerMethodArgumentResolver {

    private final NoodleRequestMapper requestMapper;

    public NoodleArgumentResolver(NoodleRequestMapper requestMapper){
        this.requestMapper = requestMapper;
    }

    @Override
    public boolean supportsParameter(MethodParameter methodParameter){
        return methodParameter.getParameterAnnotation(NoodleFilter.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory webDataBinderFactory){


        RestFilter restFilter = new RestFilter();
        HttpServletRequest r = nativeWebRequest.getNativeRequest(HttpServletRequest.class);
        if(r == null){
            return new NoodleRestFilter(restFilter, requestMapper.noodleFactory());
        }
        String json = "";
        try{
            json = IOUtils.toString(r.getInputStream(), r.getCharacterEncoding());
        }catch(IOException ignore){
        }
        try{
            NoodleFilter af = methodParameter.getParameterAnnotation(NoodleFilter.class);
            if(! json.isBlank() && af != null){
                Map<String,Object> map = requestMapper.objectMapper().readValue(json, new TypeReference<HashMap<String,Object>>() {
                });
                if(af.value().isBlank()){
                    restFilter = convert(map,af);
                }else{
                    final HashMap<String,Object> field = Optional
                            .ofNullable(map.get(af.value()))
                            .map(i -> requestMapper.objectMapper().convertValue(i, new TypeReference<HashMap<String,Object>>() {
                            }))
                            .orElse(null);
                    if(field != null){
                        restFilter = convert(map,af);
                    }
                }

            }
        }catch(IOException ignore){
            ignore.printStackTrace();
        }


        return new NoodleRestFilter(restFilter, requestMapper.noodleFactory());
    }

    RestFilter convert(Map<String,Object> map, NoodleFilter af){
        final ObjectMapper configure = requestMapper.objectMapper().copy().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        final RestFieldConverter.Store store = new RestFieldConverter.Store();
        requestMapper.customizeRestFieldConverterStore(store);
        store.build(Optional.of(af.criteriaFieldNamePolicy()).filter(i -> !i.isEmpty()).orElse(requestMapper.getDefaultFieldNamePolicy())).customizeMapper(configure);
        return requestMapper.read(map,configure);

    }
}
