package paa.coder.noodleCriteriaBuilder.interfaces;

import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.CoalesceExpression;

public interface CoalesceBuilder{

    CoalesceExpression by(String field);
    CoalesceExpression subquery(SubqueryBuilder<?> subqueryBuilder, Class<?> clazz);
}
