package paa.coder.noodleCriteriaBuilder.queryBuilder;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Author;
import paa.coder.noodleCriteriaBuilder.app.models.Author2;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.app.models.Book2;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@DataSet(cleanBefore = true)
class SampleQueryTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void where(){
        SampleQuery<Book> where = factory().select(Book.class).where(w -> w.where("name", "#info.comment"));
        assertThat(SQLExtractor.from(where.query())).endsWith("where book0_.name=book0_.comment");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(1).first().isInstanceOf(Book.class).hasFieldOrPropertyWithValue("id", 2);
    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void order(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).order(ob -> ob.asc("id"));
        assertThat(SQLExtractor.from(where.query())).endsWith("from book book0_ order by book0_.id asc");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(3);
    }


    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void stream(){
        SampleQuery<Book> where = factory().select(Book.class).order(o -> o.asc("id"));
        assertThat(where.stream().collect(Collectors.toList())).hasSize(3);
        assertThat(where.stream(1).collect(Collectors.toList())).hasSize(1).first().isInstanceOf(Book.class).hasFieldOrPropertyWithValue("id", 1);
        assertThat(where.stream(1, 1).collect(Collectors.toList())).hasSize(1).first().isInstanceOf(Book.class).hasFieldOrPropertyWithValue("id", 2);
    }

    @Test
    @DataSet(value = "sample/queryTestEager.yml", transactional = true)
    void whereEagerManyToOne(){
        SampleQuery<Book2> book = factory().select(Book2.class);
        assertThat(SQLExtractor.from(book.query())).endsWith("from book2 book2x0_ inner join author2 author2x1_ on book2x0_.author_id=author2x1_.id");
        assertThat(book.stream(2).collect(Collectors.toList())).hasSize(2);
    }

    @Test
    @DataSet(value = "sample/queryTestEager.yml", transactional = true)
    void whereEagerOneToMany(){
        SampleQuery<Author2> author = factory().select(Author2.class).order(b->b.asc("id"));
        assertThat(SQLExtractor.from(author.query())).endsWith("from author2 author2x0_ inner join book2 books1_ on author2x0_.id=books1_.author_id order by author2x0_.id asc");
        List<Author2> collect = author.stream(1).collect(Collectors.toList());
        assertThat(collect).hasSize(1);
        assertThat(collect.get(0).getBooks()).hasSize(2);
    }

    @Test
    void testOrderFromSampleToMulty(){
        SampleQuery<Author2> author = factory().select(Author2.class).order(b->b.asc("id"));
        assertThat(SQLExtractor.from(author.query())).endsWith("from author2 author2x0_ inner join book2 books1_ on author2x0_.id=books1_.author_id order by author2x0_.id asc");

        final MultiSelectQuery<Author2> id = author.select(s -> s.select("id"));
        assertThat(SQLExtractor.from(id.query())).isEqualTo("select author2x0_.id as col_0_0_ from author2 author2x0_ order by author2x0_.id asc");

    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void withoutGraph(){
        Author where = factory().select(Author.class).where(w->w.where("id",1)).stream(1).findFirst().orElseThrow();
        final Set<Book> books = where.getBooks();
    }
    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void withGraph(){
        Author where = factory().select(Author.class).withGraph("authorWithBooks").stream(1).findFirst().orElseThrow();
        final Set<Book> books = where.getBooks();
    }
}