package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DataSet(cleanBefore = true)
class ConcatExpressionTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/stringExpressionTest.yml")
    void concat(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.concat("conCat", se -> se.addField("name").add("-").add(b -> b.addField("text").add(":", 0, ":").addField("comment"))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select (((''||sample0_.name)||'-')||(((((''||sample0_.text)||':')||'0')||':')||sample0_.comment)) as col_0_0_ from sample" +
                        " sample0_");

        assertThat(select.stream().findFirst().map(i -> i.get("conCat").toString())).isNotEmpty().get().isEqualTo("name-text:0:comment");
        MultiSelectQuery<Sample> select1 = factory()
                .select(Sample.class)
                .select(s -> s.concat("conCat", se -> se.addField("name").add("-").addField("id")));
        assertThat(SQLExtractor.from(select1.query())).isEqualTo(
                "select (((''||sample0_.name)||'-')||cast(sample0_.id as varchar(255))) as col_0_0_ from sample sample0_");

        assertThat(select1.stream().findFirst().map(i -> i.get("conCat").toString())).isNotEmpty().get().isEqualTo("name-1");
    }

    @Test
    @DataSet(value = "sample/coalesceExpressionTest.yml")
    void coalesce(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.concat("conCat", se -> se.coalesce(b->b.by("rate").orMath(ab->ab.from("id").plus(1))).add("-").addField("name")));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select (((''||cast(coalesce(sample0_.rate, sample0_.id+1) as varchar(255)))||'-')||sample0_.name) as col_0_0_ from sample sample0_");
        assertThat(select.stream().findFirst().map(i -> i.get("conCat").toString())).isNotEmpty().get().isEqualTo("2.00-first");
    }
}