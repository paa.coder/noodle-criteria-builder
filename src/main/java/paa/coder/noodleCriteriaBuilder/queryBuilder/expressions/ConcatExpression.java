package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;
import paa.coder.noodleCriteriaBuilder.interfaces.CoalesceBuilder;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.interfaces.ThreeFunction;

import javax.persistence.criteria.CommonAbstractCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class ConcatExpression implements NoodleExpression {

    private ThreeFunction<PathFinder,CommonAbstractCriteria,CriteriaBuilder,Expression<String>> expresion = (r,query, c) -> c.literal("");

    public ConcatExpression(){
    }

    private ConcatExpression(ThreeFunction<PathFinder,CommonAbstractCriteria,CriteriaBuilder,Expression<String>> expresion){
        this.expresion = expresion;
    }

    public ThreeFunction<PathFinder,CommonAbstractCriteria,CriteriaBuilder,Expression<String>> getExpresion(){
        return expresion;
    }

    @Override
    public Optional<Expression<?>> apply(PathFinder pathFinder, CommonAbstractCriteria query, CriteriaBuilder criteriaBuilder){
        return Optional.ofNullable(expresion).map(i->i.apply(pathFinder, query,criteriaBuilder));
    }

    private static Path<?> path(PathFinder pathFinder, String fieldName){
        return pathFinder
                .apply(SampleColumn.build(fieldName).getName())
                .orElseThrow(() -> new NoodleException(String.format("Field %s not found", fieldName)));
    }

    private ConcatExpression actionObject(ThreeFunction<Expression<String>,Expression<String>,CriteriaBuilder,Expression<String>> updater,
                                          Object... fields){
        if(fields.length == 0){
            return this;
        }
        return new ConcatExpression((root,query, criteriaBuilder) -> {
            Expression<String> expression = expresion.apply(root,query, criteriaBuilder);
            for(Object o : fields){
                expression = updater.apply(expression, criteriaBuilder.literal(o.toString()), criteriaBuilder);
            }
            return expression;
        });
    }



    private ConcatExpression action(ThreeFunction<Expression<String>,Path<?>,CriteriaBuilder,Expression<String>> updater,
                                    String... fields){
        if(fields.length == 0){
            return this;
        }
        return new ConcatExpression((root,query, criteriaBuilder) -> {
            Expression<String> expression = expresion.apply(root,query, criteriaBuilder);
            for(Path<?> path : Stream.of(fields).map(i -> path(root, i)).toArray(Path[]::new)){
                expression = updater.apply(expression, path, criteriaBuilder);
            }
            return expression;
        });
    }

    private ConcatExpression action(ThreeFunction<Expression<String>,Expression<String>,CriteriaBuilder,Expression<String>> updater,
                                    Function<ConcatExpression,ConcatExpression> builderFunction){
        return new ConcatExpression((root,query, criteriaBuilder) -> {
            Expression<String> expression = expresion.apply(root,query, criteriaBuilder);
            return Optional
                    .ofNullable(builderFunction.apply(new ConcatExpression()))
                    .flatMap(i -> Optional.ofNullable(i.getExpresion()))
                    .flatMap(i -> Optional.ofNullable(i.apply(root,query, criteriaBuilder)))
                    .map(i -> updater.apply(expression, i, criteriaBuilder))
                    .orElse(expression);
        });
    }

    public ConcatExpression addField(String field){
        return action((e, p, c) -> c.concat(e, p.as(String.class)), field);
    }

    public ConcatExpression add(Object... field){
        return actionObject((e, p, c) -> c.concat(e, p), field);
    }

    public ConcatExpression add(Function<ConcatExpression,ConcatExpression> builderFunction){
        return action((l, r, c) -> c.concat(l, r), builderFunction);
    }

    public ConcatExpression coalesce(Function<CoalesceBuilder,CoalesceExpression> uFunc){
        return action((l, r, c) -> c.concat(l, r),_b->new ConcatExpression((pf,query,cb)->uFunc
                .apply(new CoalesceExpression.Builder())
                .getExpresion()
                .apply(s -> Optional.of(path(pf, s)),query, cb).as(String.class)));

    }
}
