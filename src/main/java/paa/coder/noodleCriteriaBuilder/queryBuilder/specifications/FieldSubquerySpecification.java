package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import paa.coder.noodleCriteriaBuilder.exceptions.ComparisonOperatorsNotSupport;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SampleColumn;

import javax.persistence.criteria.CommonAbstractCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;

public class FieldSubquerySpecification<T> extends SubquerySpecification<T>{

    private final SampleColumn field;

    public FieldSubquerySpecification(String field, String operator, SubqueryBuilder<T> queryBuilder){
        this(field, operator,queryBuilder,false);
    }
    public FieldSubquerySpecification(String field, String operator, SubqueryBuilder<T> queryBuilder, Boolean isNot){
        super(queryBuilder,operator, isNot);
        this.field = SampleColumn.build(field,null);

    }

    @Override
    protected Expression<?> getExpression(PathFinder pathFinder, CommonAbstractCriteria criteriaQuery, CriteriaBuilder criteriaBuilder){
        return pathFinder
                .apply(field.getName())
                .orElseThrow(()->new ComparisonOperatorsNotSupport(String.format("field %s not found ", field.getName())));
    }
}
