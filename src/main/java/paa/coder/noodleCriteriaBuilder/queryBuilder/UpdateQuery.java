package paa.coder.noodleCriteriaBuilder.queryBuilder;

import org.hibernate.query.Query;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SetterBuilder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.specifications.NoodleSpecificationBuilder;

import javax.persistence.criteria.*;
import java.util.function.Function;

public class UpdateQuery<T> extends NoodleAbstractQuery {

    protected final Class<T> from;
    private final SetterBuilder setterBuilder;

    public UpdateQuery(Class<T> from, NoodleFactory noodleFactory){
        super(noodleFactory);
        this.from = from;
        this.setterBuilder = new SetterBuilder(noodleFactory);
    }

    @Override
    public UpdateQuery<T> where(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> fPred){
        super.where(fPred);
        return this;
    }

    private <X> void set(Path<X> p, Expression<?> expression, CriteriaUpdate<T> update){
        update.set(p, expression.as(p.getJavaType()));
    }

    public UpdateQuery<T> set(Function<SetterBuilder,SetterBuilder> uFunc){
        uFunc.apply(setterBuilder);
        return this;
    }

    public Query query(){
        CriteriaUpdate<T> update = noodleFactory.getSession().getCriteriaBuilder().createCriteriaUpdate(from);

        Root<T> root = update.from(from);
        setterBuilder
                .getMapExpressions()
                .forEach((k, v) -> k
                        .apply(pathFinder(root), update, noodleFactory.getSession().getCriteriaBuilder())
                        .ifPresent(field -> v
                                .apply(field)
                                .apply(pathFinder(root), update, noodleFactory.getSession().getCriteriaBuilder())
                                .ifPresent(exp -> set(field, exp, update))));

        getWherePredicates(root, update, noodleFactory.getSession().getCriteriaBuilder()).ifPresent(update::where);
        return noodleFactory.getSession().createQuery(update);
    }

    public Integer updateForce(){
        return query().executeUpdate();
    }

    public Integer update(){
        find(from).forEach(i->noodleFactory.getSession().detach(i));
        return  updateForce();
    }

}
