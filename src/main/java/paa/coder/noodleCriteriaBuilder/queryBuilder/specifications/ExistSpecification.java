package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;

import javax.persistence.criteria.*;

public class ExistSpecification<T> implements NoodlePredicate {

    private final SubqueryBuilder<T> queryBuilder;
    protected final Boolean isNot;

    public ExistSpecification(SubqueryBuilder<T> queryBuilder, Boolean isNot){
        this.queryBuilder=queryBuilder;
        this.isNot = isNot;
    }


    @Override
    public Predicate apply(PathFinder pathFinder, CommonAbstractCriteria criteriaQuery, CriteriaBuilder criteriaBuilder){
        final Subquery<?> build = SubqueryBuilder.NoodleSubquery.sample(queryBuilder, pathFinder, criteriaQuery).build();
        final Predicate exists = criteriaBuilder.exists(build);
        return isNot ? exists.not() : exists;
    }
}
