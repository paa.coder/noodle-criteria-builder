package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Author;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@DataSet(cleanBefore = true)
class CoalesceExpressionTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/coalesceExpressionTest.yml")
    void thenOr(){
        MultiSelectQuery<Sample> select = factory().select(Sample.class).select(s -> s.coalesce("test", b -> b.by("volume").or("rate", "cost")));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select coalesce(coalesce(sample0_.volume, sample0_.rate), sample0_.cost) as col_0_0_ from sample sample0_");

        assertThat(select.stream().findFirst().map(i->((BigDecimal)i.get("test")).doubleValue())).isNotEmpty().get().isEqualTo(10D);
    }

    @Test
    @DataSet(value = "sample/coalesceExpressionTest.yml")
    void thenOrMath(){
        MultiSelectQuery<Sample> select = factory().select(Sample.class).select(s -> s.coalesce("test", b -> b.by("volume").orMath(_b->_b.from("cost").divide(2))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select coalesce(sample0_.volume, sample0_.cost/2) as col_0_0_ from sample sample0_");

        assertThat(select.stream().findFirst().map(i->((BigDecimal)i.get("test")).doubleValue())).isNotEmpty().get().isEqualTo(5D);
    }

    @Test
    @DataSet(value = "sample/coalesceExpressionTest.yml")
    void thenOrConcat(){
        MultiSelectQuery<Sample> select = factory().select(Sample.class).select(s -> s.coalesce("test", b -> b.by("comment").orConcat(_b->_b.addField("name").add("-").addField("cost"))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select coalesce(sample0_.comment, (((''||sample0_.name)||'-')||cast(sample0_.cost as varchar(255)))) as col_0_0_ from sample sample0_");

        assertThat(select.stream().findFirst().map(i->i.get("test").toString())).isNotEmpty().get().isEqualTo("first-10.00");

        MultiSelectQuery<Sample> select1 = factory().select(Sample.class).select(s -> s.coalesce("test", b -> b.by("comment").orConcat(_b->_b.addField("name").add("-").addField("id"))));
        assertThat(SQLExtractor.from(select1.query())).isEqualTo(
                "select coalesce(sample0_.comment, (((''||sample0_.name)||'-')||cast(sample0_.id as varchar(255)))) as col_0_0_ from sample sample0_");

        assertThat(select1.stream().findFirst().map(i->i.get("test").toString())).isNotEmpty().get().isEqualTo("first-1");
    }

    @Test
    @DataSet(value = "sample/coalesceExpressionTest.yml")
    void or(){

        MultiSelectQuery<Sample> select = factory().select(Sample.class).select(s -> s.coalesce("test", b -> b.by("comment").orLiteral("no comment")));

        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select coalesce(sample0_.comment, 'no comment') as col_0_0_ from sample sample0_");

        assertThat(select.stream().findFirst().map(i->i.get("test").toString())).isNotEmpty().get().isEqualTo("no comment");

    }

    @Test
    @DataSet(value = "sample/queryTest.yml")
    void thenOrSubquery(){

        final SubqueryBuilder<Book> b = new SubqueryBuilder<>(factory(), Book.class, "b").where(w -> w.where("b.authorId", "#id")).select(s -> s.max("b.info.size"));

        MultiSelectQuery<Author> select = factory()
                .select(Author.class)
                .where(w -> w.where(s -> s.coalesce("size", c -> c.subquery(b, Integer.class).orLiteral(9)), "=", 9))
                .select(s -> s.select("id"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select author0_.id as col_0_0_ from author author0_ where coalesce((select max(book1_.size) from book book1_ where book1_.author_id=author0_.id), 9)=9");
        final List<NoodleResult> collect = select.stream().collect(Collectors.toList());

        assertThat(collect).isNotEmpty().hasSize(1);
    }
}