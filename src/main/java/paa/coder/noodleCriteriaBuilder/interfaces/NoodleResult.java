package paa.coder.noodleCriteriaBuilder.interfaces;

import java.util.HashMap;

public class NoodleResult extends HashMap<String,Object> {

    public <V> V cast(String key){
        return (V) get(key);
    }
}
