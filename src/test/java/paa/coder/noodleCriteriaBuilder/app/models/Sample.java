package paa.coder.noodleCriteriaBuilder.app.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "sample")
public class Sample {
    @Id
    private Integer id;
    private String name;
    private BigDecimal volume;
    private BigDecimal cost;
    private BigDecimal rate;
    private String comment;
    private String text;
}
