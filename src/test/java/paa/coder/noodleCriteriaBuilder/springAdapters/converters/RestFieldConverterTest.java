package paa.coder.noodleCriteriaBuilder.springAdapters.converters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestFilter;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestOrder;
import paa.coder.noodleCriteriaBuilder.springAdapters.NoodleRequestMapper;

import java.util.HashMap;
import java.util.Optional;
import java.util.function.Function;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


class RestFieldConverterTest {

    NoodleRequestMapper noodleRequestMapper(){
        return () -> () -> null;
    }


    void common(String fieldPolicyName, NoodleRequestMapper requestMapper, String... fields) throws JsonProcessingException{

        Function<String[],RestFilter> build = arr -> new RestFilter().where(b -> b.and(arr[0], 0)
                .and(arr[1], ">=", 3).and(arr[2], "is null", null)
                .and(_b -> _b.and(arr[3], "is null", "null").or(arr[4], 5))).addOrder(new RestOrder(arr[0],false));

        final RestFilter init = build.apply(new String[]{"test_Aa", "Test_bB", "testCc", "TestDD", "test-eE"});
        final RestFilter need = build.apply(fields);


        final String s = noodleRequestMapper().objectMapper().writeValueAsString(init);

        final HashMap<String,Object> map = noodleRequestMapper().objectMapper().readValue(s, new TypeReference<>() {});

        final ObjectMapper configure = requestMapper.objectMapper().copy().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        final RestFieldConverter.Store store = new RestFieldConverter.Store();
        requestMapper.customizeRestFieldConverterStore(store);
        store.build(Optional.of(fieldPolicyName).filter(i -> ! i.isEmpty()).orElse(requestMapper.getDefaultFieldNamePolicy())).customizeMapper(configure);


        final RestFilter restFilter = requestMapper.read(map, configure);

        assertThat(restFilter).usingRecursiveComparison().isEqualTo(need);
    }

    @Test
    void defaultTest() throws JsonProcessingException{
        common("", noodleRequestMapper(), "test_Aa", "Test_bB", "testCc", "TestDD", "test-eE");

    }

    @Test
    void lowerSnakeCaseTest() throws JsonProcessingException{
        common("", new NoodleRequestMapper() {
            @Override
            public NoodleFactory noodleFactory(){
                return () -> null;
            }

            @Override
            public String getDefaultFieldNamePolicy(){
                return RestFieldConverter.FieldNamePolicy.LOWER_SNAKE_CASE_VALUE;
            }
        }, "test_aa", "test_b_b", "test_cc", "test_dd", "test-e_e");
    }

    @Test
    void upperSnakeCaseTest() throws JsonProcessingException{
        common(RestFieldConverter.FieldNamePolicy.UPPER_SNAKE_CASE_VALUE,
               noodleRequestMapper(),
               "test_aa".toUpperCase(),
               "test_b_b".toUpperCase(),
               "test_cc".toUpperCase(),
               "test_dd".toUpperCase(),
               "test-e_e".toUpperCase());
    }

    @Test
    void snakeCaseTest() throws JsonProcessingException{
        common(RestFieldConverter.FieldNamePolicy.SNAKE_CASE_VALUE, noodleRequestMapper(), "test_Aa", "Test_b_B", "test_Cc", "Test_DD", "test-e_E");
    }

    @Test
    void camelCaseTest() throws JsonProcessingException{
        common(RestFieldConverter.FieldNamePolicy.CAMEL_CASE_VALUE, noodleRequestMapper(), "test_Aa", "TestBB", "testCc", "TestDD", "test-eE");
    }

    @Test
    void customCaseTest() throws JsonProcessingException{
        common("Test2field", new NoodleRequestMapper() {
            @Override
            public NoodleFactory noodleFactory(){
                return () -> null;
            }

            @Override
            public void customizeRestFieldConverterStore(RestFieldConverter.Store store){
                store.addFieldNamePolicy("Test2field", s -> s.replaceAll("Test", "field"));
            }
        }, "test_Aa", "field_bB", "testCc", "fieldDD", "test-eE");
    }

    @Test
    void emptyProperties(){
        final NoodleRequestMapper requestMapper = noodleRequestMapper();

        String s = "{\"query\":{\"operator\":\"AND\",\"criteria\":[{\"field\":\"name\",\"value\":\"%Белый список%\",\"operator\":\"ilike\"}]},\"pageCount\":25,\"page\":0}";
        String s2 = "{\"query\":{\"operator\":\"AND\",\"criteria\":[{\"field\":\"name\",\"value\":\"%Белый список%\",\"operator\":\"ilike\",\"query\":null}]},\"pageCount\":25,\"page\":0}";

        Function<String,RestFilter> builder = str ->{
            final HashMap<String,Object> map;
            try{
                map = noodleRequestMapper().objectMapper().readValue(s, new TypeReference<>() {});
            }catch(JsonProcessingException e){
                throw new RuntimeException(e);
            }

            final ObjectMapper configure = requestMapper.objectMapper().copy().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            final RestFieldConverter.Store store = new RestFieldConverter.Store();
            requestMapper.customizeRestFieldConverterStore(store);
            store.build(requestMapper.getDefaultFieldNamePolicy()).customizeMapper(configure);


            return requestMapper.read(map, configure);
        };

        assertThat(builder.apply(s)).isEqualTo(builder.apply(s2));
    }

    @Test
    public void sundbox() throws JsonProcessingException{
        String s ="{\n" +
                "    \"query\": {\n" +
                "        \"operator\": \"AND\",\n" +
                "        \"criteria\": [\n" +
                "            {\n" +
                "                \"query\": {\n" +
                "                    \"operator\": \"OR\",\n" +
                "                    \"criteria\": [\n" +
                "                        {\n" +
                "                            \"field\": \"createdAt\",\n" +
                "                            \"value\": \"2024-02-29 23:59:59\",\n" +
                "                            \"operator\": \"<\"\n" +
                "                        },\n" +
                "                        {\n" +
                "                            \"field\": \"createdAt\",\n" +
                "                            \"value\": \"2024-02-01 00:00:00\",\n" +
                "                            \"operator\": \">=\"\n" +
                "                        }\n" +
                "                    ]\n" +
                "                }\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    \"order\": [\n" +
                "        {\n" +
                "            \"fields\": [],\n" +
                "            \"isAsc\": true\n" +
                "        },\n" +
                "        {\n" +
                "            \"fields\": [\n" +
                "                \"number\"\n" +
                "            ],\n" +
                "            \"isAsc\": false\n" +
                "        }\n" +
                "    ],\n" +
                "    \"pageCount\": 25,\n" +
                "    \"page\": 0\n" +
                "}";

        final HashMap<String,Object> map = noodleRequestMapper().objectMapper().readValue(s, new TypeReference<>() {});
        final ObjectMapper configure = noodleRequestMapper().objectMapper().copy().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        final RestFieldConverter.Store store = new RestFieldConverter.Store();
        noodleRequestMapper().customizeRestFieldConverterStore(store);
        store.build(noodleRequestMapper().getDefaultFieldNamePolicy()).customizeMapper(configure);
        final RestFilter restFilter = noodleRequestMapper().read(map, configure);

        System.out.println("xajk");

    }


}