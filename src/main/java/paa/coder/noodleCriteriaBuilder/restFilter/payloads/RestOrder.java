package paa.coder.noodleCriteriaBuilder.restFilter.payloads;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.OrderBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

@Data
@NoArgsConstructor
public class RestOrder {
    private List<String> fields = new ArrayList<>();
    private boolean isAsc = true;

    @JsonCreator
    public RestOrder(@JsonProperty("fields") List<String> fields, @JsonProperty("isAsc") boolean isAsc){
        this.fields.addAll(fields);
        this.isAsc = isAsc;
    }

    public RestOrder(boolean isAsc, String... fields){
        this.fields.addAll(Arrays.asList(fields));
        this.isAsc = isAsc;
    }

    public RestOrder(String... fields){
        this.fields.addAll(Arrays.asList(fields));
    }

    public RestOrder(String field, boolean isAsc){
        this.isAsc = isAsc;
        this.fields.add(field);
    }

    public RestOrder(String field){
        this.fields.add(field);
    }


    public RestOrder(ArrayList<String> fields){
        this.fields = fields;
    }


    public void build(OrderBuilder o, Predicate<String> fieldChecker,
                      Function<String,Optional<Function<OrderBuilder.ExpressionBuilder,List<NoodleExpression>>>> handler){
        fields.stream().filter(fieldChecker).forEach(f -> {
            Optional<Function<OrderBuilder.ExpressionBuilder,List<NoodleExpression>>> ouFunc = handler.apply(f);
            if(ouFunc == null){
                o.add(isAsc, f);
                return;
            }
            ouFunc.ifPresent(i -> o.addExpressions(isAsc, i));
        });
    }
}
