package paa.coder.noodleCriteriaBuilder.restFilter;


import lombok.Data;
import org.hibernate.query.Query;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.NoodleUtils;
import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleQuery;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SampleQuery;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.OrderBuilder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.specifications.NoodleSpecificationBuilder;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestCriteria;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestFilter;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestPage;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
public class NoodleRestFilter {

    private final RestFilter restFilter;
    private final NoodleFactory noodleFactory;
    private final Set<String> disableFields = new HashSet<>();
    private final Set<String> enableFields = new HashSet<>();
    private final Map<String,Optional<BiFunction<RestCriteria,NoodlePredicate.Builder,NoodleSpecificationBuilder>>> criteriaHandler = new HashMap<>();
    private final Map<String,Optional<Function<OrderBuilder.ExpressionBuilder,List<NoodleExpression>>>> orderHandler
            = new HashMap<>();
    private final List<Function<NoodlePredicate.Builder,NoodleSpecificationBuilder>> predicates = new ArrayList<>();

    public NoodleRestFilter(RestFilter restFilter, NoodleFactory noodleFactory){
        this.restFilter = Optional.ofNullable(restFilter).orElse(new RestFilter());
        this.noodleFactory = noodleFactory;
    }

    public NoodleRestFilter disableFields(String... fields){
        disableFields.addAll(Set.of(fields));
        return this;
    }

    public NoodleRestFilter disableAllFields(Class<?> clazz){
        disableFields.addAll(NoodleUtils.getEntityColumnNames(clazz));
        return this;
    }

    public NoodleRestFilter enableFields(String... fields){
        enableFields.addAll(Set.of(fields));
        return this;
    }

    public NoodleRestFilter and(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> uFunc){
        predicates.add(uFunc);
        return this;
    }

    public NoodleRestFilter withHandler(String field,
                                        BiFunction<RestCriteria,NoodlePredicate.Builder,NoodleSpecificationBuilder> handler){
        criteriaHandler.put(field, Optional.ofNullable(handler));
        return this;
    }

    @Deprecated
    public NoodleRestFilter withOrderHandler(String field,
                                             Function<OrderBuilder.ExpressionBuilder,Optional<NoodleExpression>> handler){


        return addOrderHandler(field,
                               Optional
                                       .ofNullable(handler)
                                       .map(h -> (Function<OrderBuilder.ExpressionBuilder,List<NoodleExpression>>) ob -> h
                                               .apply(ob)
                                               .stream()
                                               .collect(
                                                       Collectors.toList()))
                                       .orElse(null));
    }

    public NoodleRestFilter addOrderHandler(String field,
                                            Function<OrderBuilder.ExpressionBuilder,List<NoodleExpression>> handler){
        orderHandler.put(field, Optional.ofNullable(handler));
        return this;
    }

    private Predicate<String> fieldChecker(){
        return str -> disableFields.stream().noneMatch(str::startsWith) || enableFields
                .stream()
                .anyMatch(str::startsWith);
    }

    public NoodleSpecificationBuilder specification(NoodlePredicate.Builder b){
        Function<String,Boolean> fieldThrower = str -> {
            if(fieldChecker().test(str)){
                return true;
            }
            throw new NoodleException.FieldBlocked("Поле не доступно для фильтрации", null, str);
        };

        NoodleSpecificationBuilder nsb = restFilter
                .getQuery()
                .build(b, fieldThrower, c -> criteriaHandler.get(c.getField()));
        predicates.forEach(nsb::and);
        return nsb;
    }

    public <T> SampleQuery<T> noodleQuery(Class<T> tClass){
        return noodleFactory
                .select(tClass)
                .where(this::specification)
                .order(o -> restFilter
                        .getOrder()
                        .forEach(restOrder -> restOrder.build(o, fieldChecker(), orderHandler::get)));
    }

    private <T, V> NoodleQuery<?,V> noodleQuery(Class<T> tClass, Function<SampleQuery<T>,NoodleQuery<?,V>> uFunc){
        return uFunc.apply(noodleQuery(tClass));
    }

    public <T> Query<?> query(Class<T> tClass){
        return query(tClass, n -> n);
    }

    public <T, V> Query<?> query(Class<T> tClass, Function<SampleQuery<T>,NoodleQuery<?,V>> uFunc){
        return query(noodleQuery(tClass, uFunc).criteriaQuery(tClass.getSimpleName()));
    }

    private Query<?> query(CriteriaQuery<?> criteriaQuery){
        return noodleFactory.getSession().createQuery(criteriaQuery);
    }

    @Deprecated
    public <T> Stream<T> stream(Class<T> tClass){
        return noodleQuery(tClass, i -> i).stream();
    }

    public <T> RestPage<T> build(Class<T> tClass){
        return build(tClass, i -> i);
    }

    public <T> RestPage<T> buildWithGraph(Class<T> tClass,String graph){
        return build(tClass, i -> i.withGraph(graph));
    }

    public <T, V> RestPage<V> build(Class<T> tClass, Function<SampleQuery<T>,NoodleQuery<?,V>> uFunc){


        String alias = String.format("nood_%s", tClass.getSimpleName().toLowerCase());

        Function<CriteriaQuery<?>,Long> count = query -> {
            CriteriaQuery<Long> countQuery = noodleFactory.getSession().getCriteriaBuilder().createQuery(Long.class);
            Root<T> entity_ = countQuery.from(tClass);
            entity_.alias(alias); //use the same alias in order to match the restrictions part and the selection part
            countQuery.select(noodleFactory.getSession().getCriteriaBuilder().count(entity_));
            Optional.ofNullable(query.getRestriction()).ifPresent(countQuery::where);
            return noodleFactory.getSession().createQuery(countQuery).getSingleResult();
        };
        NoodleQuery<?,V> noodleQuery = noodleQuery(tClass, uFunc);
        List<V> list;
        if(restFilter.getPageCount() != null){
            list = noodleQuery
                    .stream(restFilter.getPageCount(), restFilter.getPage() * restFilter.getPageCount())
                    .collect(Collectors.toList());
        }else{
            list = noodleQuery.stream().collect(Collectors.toList());
        }

        return new RestPage<V>(restFilter, list, count.apply(noodleQuery.criteriaQuery(alias)));
    }
}
