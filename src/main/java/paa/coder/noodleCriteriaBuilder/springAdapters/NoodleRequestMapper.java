package paa.coder.noodleCriteriaBuilder.springAdapters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestFilter;
import paa.coder.noodleCriteriaBuilder.springAdapters.converters.RestFieldConverter;

import java.util.Map;

public interface NoodleRequestMapper {

    NoodleFactory noodleFactory();

    default RestFilter read(@NotNull Map<String,Object> o,ObjectMapper objectMapper){
        return objectMapper.convertValue(o, RestFilter.class);
    }

    default ObjectMapper objectMapper(){
        return noodleFactory().getObjectMapper();
    }

    default String getDefaultFieldNamePolicy(){
        return RestFieldConverter.FieldNamePolicy.DEFAULT_VALUE;
    }

    default void customizeRestFieldConverterStore(RestFieldConverter.Store store){}

}
