package paa.coder.noodleCriteriaBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.github.database.rider.junit5.DBUnitExtension;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.query.criteria.internal.compile.ExplicitParameterInfo;
import org.hibernate.query.internal.QueryImpl;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import paa.coder.noodleCriteriaBuilder.app.serializers.BigDecimalDeserializer;
import paa.coder.noodleCriteriaBuilder.app.serializers.BigDecimalSerializer;
import paa.coder.noodleCriteriaBuilder.app.serializers.NoodleInstantDeserializer;
import paa.coder.noodleCriteriaBuilder.app.serializers.NoodleInstantSerializer;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@ExtendWith(DBUnitExtension.class)
@RunWith(JUnitPlatform.class)
public abstract class DbContext {

    protected Object getParamValue(String p, Query<?> query){
        if(query instanceof QueryImpl){
            return query.getParameterValue(p);
        }
        return query.getParameterValue(new ExplicitParameterInfo<>(p, null, Object.class));
    }

    public ObjectMapper objectMapper(){
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("InstantModule");
        module.addSerializer(Instant.class, new NoodleInstantSerializer());
        module.addDeserializer(Instant.class, new NoodleInstantDeserializer());
        mapper.registerModule(module);

        SimpleModule moduleBigDecimal = new SimpleModule("BigDecimalModule");
        moduleBigDecimal.addSerializer(BigDecimal.class, new BigDecimalSerializer());
        moduleBigDecimal.addDeserializer(BigDecimal.class, new BigDecimalDeserializer());
        mapper.registerModule(moduleBigDecimal);

        mapper.registerModule(new Hibernate5Module());

        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setDateFormat(new StdDateFormat());
        return mapper;
    }

    public NoodleFactory factory(){
        return new NoodleFactory() {
            @Override
            public SessionFactory sessionFactory(){
                return getSession().getSessionFactory();
            }

            @Override
            public Session getSession(){
                return  EntityManagerProvider.em().unwrap(Session.class);
            }

            @Override
            public ObjectMapper getObjectMapper(){
                return objectMapper();
            }
        };
    }

    public DateTimeFormatter getDateFormatter(){
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("UTC"));
    }
}
