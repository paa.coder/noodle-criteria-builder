package paa.coder.noodleCriteriaBuilder.queryBuilder;

import org.hibernate.query.Query;
import org.hibernate.query.criteria.internal.path.SingularAttributePath;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleQuery;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.OrderBuilder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SelectStore;
import paa.coder.noodleCriteriaBuilder.queryBuilder.specifications.NoodleSpecificationBuilder;

import javax.persistence.EntityGraph;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SampleQuery<T> extends NoodleAbstractQuery.Select implements NoodleQuery<T,T> {

    protected final Class<T> from;
    protected final OrderBuilder orderBuilder;

    protected String graphName;

    public SampleQuery(Class<T> from, NoodleFactory noodleFactory){
        super(noodleFactory);
        this.orderBuilder = new OrderBuilder();
        this.from = from;
    }

    public SampleQuery<T> where(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> fPred){
        super.where(fPred);
        return this;
    }

    public SampleQuery<T> withGraph(String graph){
        this.graphName = graph;
        return this;
    }

    public SampleQuery<T> order(Consumer<OrderBuilder> fPred){
        fPred.accept(orderBuilder);
        return this;
    }

    public MultiSelectQuery<T> select(Consumer<SelectStore.Multiply> action){
        MultiSelectQuery<T> m = initMulti();
        m.select(action);
        return m;
    }

    @Override
    public SampleQuery<T> itDistinct(Boolean d){
        super.itDistinct(d);
        return this;
    }

    public MultiSelectQuery<T> having(Function<NoodlePredicate.Builder,NoodleSpecificationBuilder> action){
        MultiSelectQuery<T> m = initMulti();
        m.having(action);
        return m;
    }

    public MultiSelectQuery<T> group(Consumer<SelectStore> action){
        return group(action, false);
    }

    public MultiSelectQuery<T> group(Consumer<SelectStore> action, Boolean addToSelect){
        MultiSelectQuery<T> m = initMulti();
        m.group(action, addToSelect);
        return m;
    }

    private MultiSelectQuery<T> initMulti(){
        return new MultiSelectQuery<>(from, where, noodleFactory,orderBuilder);
    }

    public Stream<T> stream(Integer count, Integer offset){
        TypedQuery<T> query;

        if(!noodleFactory.fetchStore().getOneToManyFetches(from).isEmpty() || !noodleFactory.fetchStore().getManyToOneFetches(from).isEmpty()){
            query = getByIds(count, offset);
        }else{
            query = query();
            Optional.ofNullable(count).ifPresent(i -> {
                query.setMaxResults(i);
                Optional.ofNullable(offset).ifPresent(query::setFirstResult);
            });
        }

        if(graphName!=null){
            EntityGraph<?> entityGraph = noodleFactory.getSession().getEntityGraph(graphName);
            query.setHint("javax.persistence.fetchgraph", entityGraph);
            return query.getResultList().stream();
        }

        return query.getResultStream();
    }

    public Query<T> query(){
        return noodleFactory.getSession().createQuery(criteriaQuery());
    }

    public CriteriaQuery<T> criteriaQuery(String alias){
        CriteriaBuilder criteriaBuilder = noodleFactory.getSession().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(from);
        Root<T> root = criteriaQuery.from(from);
        root.alias(alias);

        noodleFactory.fetchStore().getManyToOneFetches(from).forEach(i -> root.fetch(i, JoinType.INNER));
        noodleFactory.fetchStore().getOneToManyFetches(from).forEach(i -> root.fetch(i, JoinType.INNER));

        criteriaQuery.select(root);
        criteriaQuery.orderBy(orderBuilder.apply(pathFinder(root), criteriaQuery, criteriaBuilder));
        build(root, criteriaQuery, criteriaBuilder);
        return criteriaQuery;
    }

    public Query<T> getByIds(Integer count, Integer offset){

        Query<Tuple> idsQuery = getIdsQuery();
        Optional.ofNullable(count).ifPresent(i -> {
            idsQuery.setMaxResults(i);
            Optional.ofNullable(offset).ifPresent(idsQuery::setFirstResult);
        });
        List<Tuple> ids = idsQuery.getResultList();

        CriteriaQuery<T> queryResult = noodleFactory.getSession().getCriteriaBuilder().createQuery(from);
        Root<T> rootResult = queryResult.from(from);

        noodleFactory.fetchStore().getManyToOneFetches(from).forEach(i -> rootResult.fetch(i,JoinType.LEFT));
        noodleFactory.fetchStore().getOneToManyFetches(from).forEach(i -> rootResult.fetch(i,JoinType.LEFT));

        queryResult.select(rootResult);

        CriteriaBuilder cb = noodleFactory.getSession().getCriteriaBuilder();

        queryResult.orderBy(orderBuilder.apply(pathFinder(rootResult), queryResult, cb));

        Predicate[] predicatesOr = ids.stream().map(i -> {
            Predicate[] predicates = IntStream.range(0, i.getElements().size()).mapToObj(x -> {
                SingularAttributePath<?> tupleElement = (SingularAttributePath) i.getElements().get(x);
                return cb.equal(tupleElement, i.get(x));
            }).toArray(Predicate[]::new);
            if(predicates.length > 0){
                return cb.and(predicates);
            }
            return null;
        }).filter(Objects::nonNull).toArray(Predicate[]::new);

        if(predicatesOr.length > 0){
            queryResult.where(cb.or(predicatesOr));
        }else{
            queryResult.where(cb.notEqual(cb.literal(1), cb.literal(1)));
        }

        return noodleFactory.getSession().createQuery(queryResult);
    }

    public Query<Tuple> getIdsQuery(){
        CriteriaBuilder cb = noodleFactory.getSession().getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = cb.createQuery(Tuple.class);

        Root<T> root = criteriaQuery.from(from);
        List<Selection<?>> collect = noodleFactory.fetchStore().getIds(from).stream().map(root::get).collect(Collectors.toList());
        criteriaQuery.multiselect(collect);

        build(root, criteriaQuery, cb);
        criteriaQuery.orderBy(orderBuilder.apply(pathFinder(root), criteriaQuery, cb));
        return noodleFactory.getSession().createQuery(criteriaQuery);
    }

    public CriteriaQuery<T> criteriaQuery(){
        return criteriaQuery(null);
    }

}
