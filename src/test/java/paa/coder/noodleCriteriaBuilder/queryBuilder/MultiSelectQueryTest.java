package paa.coder.noodleCriteriaBuilder.queryBuilder;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import lombok.Data;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;

import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@DataSet(cleanBefore = true)
class MultiSelectQueryTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void where(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("name", "#info.comment"));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name=book0_.comment");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(1).first().isInstanceOf(NoodleResult.class).hasFieldOrPropertyWithValue("id", 2);
    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void order(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).order(ob -> ob.asc("id"));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ order by book0_.id asc");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(3);
    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void select(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id"));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(3);
    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void having(){
        MultiSelectQuery<Book> where = factory()
                .select(Book.class)
                .select(s -> s.count("id as countId"))
                .group(g -> g.select("authorId"), true)
                .having(h -> h.where("authorId", 2));
        assertThat(SQLExtractor.from(where.query())).isEqualTo(
                "select count(book0_.id) as col_0_0_, book0_.author_id as col_1_0_ from book book0_ group by book0_.author_id having book0_.author_id=2");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(1);
    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void itDistinct(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("authorId")).itDistinct(true);
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select distinct book0_.author_id as col_0_0_ from book book0_");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(2);
    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void group(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.count("id as countId")).group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select count(book0_.id) as col_0_0_ from book book0_ group by book0_.author_id");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(2);
    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void groupAndSelect(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.count("id as countId")).group(g -> g.select("authorId"), true);
        assertThat(SQLExtractor.from(where.query())).isEqualTo(
                "select count(book0_.id) as col_0_0_, book0_.author_id as col_1_0_ from book book0_ group by book0_.author_id");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(2);
    }


    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void stream(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).order(o->o.asc("id"));
        assertThat(where.stream().collect(Collectors.toList())).hasSize(3);
        assertThat(where.stream(1).collect(Collectors.toList())).hasSize(1).first().isInstanceOf(NoodleResult.class).hasFieldOrPropertyWithValue("id", 1);
        assertThat(where.stream(1,1).collect(Collectors.toList())).hasSize(1).first().isInstanceOf(NoodleResult.class).hasFieldOrPropertyWithValue("id", 2);

    }

    @Test
    @DataSet(value = "sample/queryTest.yml", transactional = true)
    void projection(){
        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("name as name","info.comment as comment")).order(o->o.asc("id"));
        assertThat(where.projection(ProjectionTest.class,1,1)).hasSize(1).first().isInstanceOf(ProjectionTest.class)
                .hasFieldOrPropertyWithValue("name","The Man in the High Castle")
                .hasFieldOrPropertyWithValue("comment","The Man in the High Castle");
    }

    @Data
    public static class ProjectionTest{
        private String name;
        private String comment;
    }
}