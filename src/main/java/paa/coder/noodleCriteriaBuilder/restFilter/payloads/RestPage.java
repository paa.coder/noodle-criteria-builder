package paa.coder.noodleCriteriaBuilder.restFilter.payloads;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@NoArgsConstructor
public class RestPage<T> {
    private RestFilter filter;
    private List<T> content;
    private long totalElements;

    public RestPage(RestFilter filter, List<T> content, Long totalElements){
        this.filter = filter;
        this.content = Optional.ofNullable(content).orElse(new ArrayList<>());
        this.totalElements = totalElements;
    }

    public <W>RestPage<W> map(Function<? super T, ? extends W> converter){
        return new RestPage<W>(filter,content.stream().map(converter).collect(Collectors.toList()), totalElements);
    }

    public Stream<T> stream(){
        return content.stream();
    }
}
