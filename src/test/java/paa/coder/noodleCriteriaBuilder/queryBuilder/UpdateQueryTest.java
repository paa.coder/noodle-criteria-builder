package paa.coder.noodleCriteriaBuilder.queryBuilder;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class UpdateQueryTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/updateQueryTest.yml", transactional = true)
    void update(){
        UpdateQuery<Book> updateQuery = factory().update(Book.class).set(s->s.set("info.size",1));
        assertThat(SQLExtractor.from(updateQuery.query())).isEqualTo("update book set size=1");
        Assertions.assertThrows(NoodleException.class, updateQuery::update);

        updateQuery.where(w -> w.where("info.size", "is null",(Object) null));
        assertThat(SQLExtractor.from(updateQuery.query())).isEqualTo("update book set size=1 where size is null");
        assertThat(updateQuery.update()).isNotNull().isEqualTo(1);
    }

    @Test
    @DataSet(value = "sample/updateQueryTest.yml", transactional = true)
    void updateAll(){
        UpdateQuery<Book> updateQuery = factory().update(Book.class).set(s->s.set("info.size",1));
        assertThat(SQLExtractor.from(updateQuery.query())).isEqualTo("update book set size=1");
        assertThat(updateQuery.updateForce()).isNotNull().isEqualTo(3);
    }


}