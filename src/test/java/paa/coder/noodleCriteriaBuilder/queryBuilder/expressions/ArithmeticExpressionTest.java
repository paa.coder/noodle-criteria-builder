package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DataSet(cleanBefore = true)
class ArithmeticExpressionTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/arithmeticExpressionTest.yml")
    void thenPlus(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.math("sumOf", b -> b.from("volume").plus(- 1, 2).plus("cost", "rate").plus(_b -> _b.from(1).plus(1))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select sample0_.volume+-1+2+sample0_.cost+sample0_.rate+1+1 as col_0_0_ from sample " + "sample0_");

        assertThat(select.stream().findFirst().map(i -> ((BigDecimal) i.get("sumOf")).doubleValue())).isNotEmpty().get().isEqualTo(24D);
    }

    @Test
    @DataSet(value = "sample/arithmeticExpressionTest.yml")
    void thenMinus(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.math("sumOf", b -> b.from("volume").minus(- 1, 2).minus("cost", "rate").minus(_b -> _b.from(1).minus(1))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select sample0_.volume-(-1)-2-sample0_.cost-sample0_.rate-(1-1) as col_0_0_ from sample " + "sample0_");
        assertThat(select.stream().findFirst().map(i -> ((BigDecimal) i.get("sumOf")).doubleValue())).isNotEmpty().get().isEqualTo(- 2D);
    }

    @Test
    @DataSet(value = "sample/arithmeticExpressionTest.yml")
    void thenMultiply(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.math("sumOf", b -> b.from("volume").multiply(- 1, 2).multiply("cost", "rate").multiply(_b -> _b.from(1).multiply(1))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select sample0_.volume*-1*2*sample0_.cost*sample0_.rate*1*1 as col_0_0_ from sample " + "sample0_");
        assertThat(select.stream().findFirst().map(i -> ((BigDecimal) i.get("sumOf")).doubleValue())).isNotEmpty().get().isEqualTo(- 200D);
    }

    @Test
    @DataSet(value = "sample/arithmeticExpressionTest.yml")
    void thenDivide(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.math("sumOf", b -> b.from("volume").divide(- 1, 2).divide("cost", "rate").divide(_b -> _b.from(1).divide(1))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select sample0_.volume/(-1)/2/sample0_.cost/sample0_.rate/(1/1) as col_0_0_ from sample " + "sample0_");
        assertThat(select.stream().findFirst().map(i -> ((BigDecimal) i.get("sumOf")).doubleValue())).isNotEmpty().get().isEqualTo(- 0.5);
    }

    @Test
    @DataSet(value = "sample/arithmeticExpressionTest.yml")
    void thenExpression(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.math("sumOf", b -> b.from("volume").multiply(_b -> _b.from(1).plus(2).divide(__b -> __b.from(4).minus(1)))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select sample0_.volume*(1+2)/(4-1) as col_0_0_ from sample sample0_");

        assertThat(select.stream().findFirst().map(i -> ((BigDecimal) i.get("sumOf")).doubleValue())).isNotEmpty().get().isEqualTo(10D);
    }

    @Test
    @DataSet(value = "sample/arithmeticExpressionTest.yml")
    void thenCoalesce(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.math("sumOf",
                                    b -> b.coalesce(cb -> cb.by("volume").or("rate")).divide(_b -> _b.coalesce(cb -> cb.by("volume").or("rate")))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select coalesce(sample0_.volume, sample0_.rate)/coalesce(sample0_.volume, sample0_.rate) as col_0_0_ from sample sample0_");

        assertThat(select.stream().findFirst().map(i -> ((BigDecimal) i.get("sumOf")).doubleValue())).isNotEmpty().get().isEqualTo(1D);

        assertThatThrownBy(() -> factory()
                .select(Sample.class)
                .select(s -> s.math("sumOf",
                                    b -> b.coalesce(cb -> cb.by("volume").or("name")).divide(_b -> _b.coalesce(cb -> cb.by("volume").or("rate"))))).stream(1))
                .isInstanceOf(NoodleException.class)
                .hasFieldOrPropertyWithValue("message", "Field name must instance from java.lang.Number");
    }
}