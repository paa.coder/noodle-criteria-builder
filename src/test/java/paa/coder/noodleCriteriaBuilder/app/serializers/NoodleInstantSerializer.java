package paa.coder.noodleCriteriaBuilder.app.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class NoodleInstantSerializer extends StdSerializer<Instant> {

    public NoodleInstantSerializer() {
        this(Instant.class);
    }

    public NoodleInstantSerializer(Class<Instant> t) {
        super(t);
    }



    @Override
    public void serialize(Instant value, JsonGenerator generator,
                          SerializerProvider serializers) throws IOException {

        ZoneId zoneId=ZoneId.of("Europe/Samara");

        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(zoneId);
        String str = fmt.format(value);

        generator.writeStartObject();
        generator.writeFieldName("date");
        generator.writeString(str);
        generator.writeFieldName("zone");
        generator.writeString(zoneId.getId());
        generator.writeEndObject();
    }

    @Override
    public Class<Instant> handledType() {
        return Instant.class;
    }


}
