package paa.coder.noodleCriteriaBuilder.restFilter.payloads;

import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

@Data
public class RestFilter {
    private RestQuery query = new RestQuery();
    private List<RestOrder> order = new ArrayList<>();
    private Integer page = 0;
    private Integer pageCount = 500;
    private String hash;


    public RestFilter addOrder(RestOrder... orders){
        this.order.addAll(Arrays.asList(orders));
        return this;
    }

    public RestFilter where(Function<RestQuery.Builder,RestQuery.Builder> builder){
        query = builder.apply(new RestQuery.Builder(query)).getQuery();
        return this;
    }

    public RestFilter withPage(Integer page){
        this.page = page;
        return this;
    }

    public RestFilter withPageCount(Integer pageCount){
        this.pageCount = pageCount;
        return this;
    }
}
