package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.interfaces.ArithmeticBuilder;
import paa.coder.noodleCriteriaBuilder.interfaces.CoalesceBuilder;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;

import javax.persistence.criteria.CommonAbstractCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SelectStore implements NoodleExpression.Items {

    protected final List<NoodleExpression> items = new ArrayList<>();
    protected final NoodleExpression.Builder builder;

    public SelectStore(){
        this.builder = new NoodleExpression.Builder() {};
    }

    public SelectStore select(String... fields){
        items.addAll(builder.select(fields).collect(Collectors.toList()));
        return this;
    }

    @Override
    public List<Expression<?>> apply(PathFinder root, CommonAbstractCriteria query, CriteriaBuilder criteriaBuilder){
        return items.stream().map(i->i.apply(root,query,criteriaBuilder)).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
    }

    public SelectStore custom(String alias, BiFunction<Function<String,Path<?>>,CriteriaBuilder,Expression<?>> userFunc){
        items.addAll(builder.custom(alias,userFunc).collect(Collectors.toList()));
        return this;
    }

    public static class Multiply extends SelectStore{

        private final NoodleFactory factory;

        public Multiply(NoodleFactory factory){
            this.factory = factory;
        }

        public Multiply select(Class<?> clazz){
            return selectWithPrefix(null, clazz);
        }

        public Multiply selectWithPrefix(String prefix, Class<?> clazz){
            items.addAll(builder.getColumns(prefix, factory.getEntityColumnNames(clazz).toArray(new String[0])).collect(Collectors.toList()));
            return this;
        }

        @Override
        public Multiply select(String... fields){
            super.select(fields);
            return this;
        }

        public Multiply coalesce(String alias, Function<CoalesceBuilder,CoalesceExpression> userFunc){
            items.addAll(builder.coalesce(alias,userFunc).collect(Collectors.toList()));
            return this;
        }

        public Multiply custom(String alias, BiFunction<Function<String,Path<?>>,CriteriaBuilder,Expression<?>> userFunc){
            super.custom(alias,userFunc);
            return this;
        }

        public Multiply math(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            items.addAll(builder.math(alias,userFunc).collect(Collectors.toList()));
            return this;
        }

        public Multiply concat(String alias, Function<ConcatExpression,ConcatExpression> userFunc){
            items.addAll(builder.concat(alias, userFunc).collect(Collectors.toList()));
            return this;
        }

        public Multiply length(String... fields){
            items.addAll(builder.length(fields).collect(Collectors.toList()));
            return this;
        }

        public Multiply sum(String... fields){
            items.addAll(builder.sum(fields).collect(Collectors.toList()));
            return this;
        }

        public Multiply sum(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            items.addAll(builder.sum(alias,userFunc).collect(Collectors.toList()));
            return this;

        }

        public Multiply count(String... fields){
            items.addAll(builder.count(fields).collect(Collectors.toList()));
            return this;
        }

        public Multiply avg(String... fields){
            items.addAll(builder.avg(fields).collect(Collectors.toList()));
            return this;
        }

        public Multiply avg(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            items.addAll(builder.avg(alias,userFunc).collect(Collectors.toList()));
            return this;

        }

        public Multiply max(String... fields){
            items.addAll(builder.max(fields).collect(Collectors.toList()));
            return this;
        }

        public Multiply max(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            items.addAll(builder.max(alias,userFunc).collect(Collectors.toList()));
            return this;

        }

        public Multiply min(String... fields){
            items.addAll(builder.min(fields).collect(Collectors.toList()));
            return this;
        }

        public Multiply min(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            items.addAll(builder.min(alias,userFunc).collect(Collectors.toList()));
            return this;
        }

        public Multiply subquery(String alias, SubqueryBuilder.Initiator.Runner userFunc){
            NoodleExpression wrap = (pathFinder, query,criteriaBuilder) -> {
                Expression<?> expression = new SubqueryBuilder.NoodleSubquery<>(userFunc.apply(new SubqueryBuilder.Initiator(factory)),pathFinder,query.subquery(Object.class)).build();
                expression.alias(alias);
                return Optional.of(expression);
            };
            items.add(wrap);
            return this;
        }
    }

    public static class Single implements NoodleExpression{

        private NoodleExpression expression;
        protected final NoodleExpression.Builder builder;

        public Single(){this.builder = new NoodleExpression.Builder() {};}

        private void setExpression(NoodleExpression expression){
            this.expression = expression;
        }

        public NoodleExpression getExpression(){
            return expression;
        }

        public void select(String fields){
           builder.select(fields).findFirst().ifPresent(this::setExpression);
        }

        public void coalesce(String alias, Function<CoalesceBuilder,CoalesceExpression> userFunc){
            builder.coalesce(alias,userFunc).findFirst().ifPresent(this::setExpression);
        }

        public void custom(String alias, BiFunction<Function<String,Path<?>>,CriteriaBuilder,Expression<?>> userFunc){
            builder.custom(alias,userFunc).findFirst().ifPresent(this::setExpression);
        }

        public void math(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            builder.math(alias,userFunc).findFirst().ifPresent(this::setExpression);
        }

        public void concat(String alias, Function<ConcatExpression,ConcatExpression> userFunc){
            builder.concat(alias, userFunc).findFirst().ifPresent(this::setExpression);
        }

        public void length(String field){
            builder.length(field).findFirst().ifPresent(this::setExpression);
        }

        public void sum(String fields){
            builder.sum(fields).findFirst().ifPresent(this::setExpression);
        }

        public void sum(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            builder.sum(alias,userFunc).findFirst().ifPresent(this::setExpression);

        }

        public void count(String fields){
            builder.count(fields).findFirst().ifPresent(this::setExpression);
        }

        public void avg(String fields){
            builder.avg(fields).findFirst().ifPresent(this::setExpression);
        }

        public void avg(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            builder.avg(alias,userFunc).findFirst().ifPresent(this::setExpression);
        }

        public void max(String fields){
            builder.max(fields).findFirst().ifPresent(this::setExpression);
        }

        public void max(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            builder.max(alias,userFunc).findFirst().ifPresent(this::setExpression);
        }

        public void min(String... fields){
            builder.min(fields).findFirst().ifPresent(this::setExpression);
        }

        public void min(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            builder.min(alias,userFunc).findFirst().ifPresent(this::setExpression);
        }

        @Override
        public Optional<Expression<?>> apply(PathFinder pathFinder, CommonAbstractCriteria query, CriteriaBuilder criteriaBuilder){
            return Optional.ofNullable(expression).flatMap(e->e.apply(pathFinder,query,criteriaBuilder));
        }
    }
}

