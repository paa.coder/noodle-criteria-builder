package paa.coder.noodleCriteriaBuilder.interfaces;

import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.ArithmeticExpression;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.CoalesceExpression;

import java.util.function.Function;

public interface ArithmeticBuilder {
    
    ArithmeticExpression from(String field);
    ArithmeticExpression from(Number field);
    ArithmeticExpression coalesce(Function<CoalesceBuilder,CoalesceExpression> uFunc);
}
