package paa.coder.noodleCriteriaBuilder.interfaces;

import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.ArithmeticExpression;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.CoalesceExpression;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SampleColumn;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.ConcatExpression;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

public interface NoodleExpression extends NoodleSelection<Optional<? extends Expression<?>>> {

    interface Items extends NoodleSelection<List<Expression<?>>> {
    }

    interface Builder {

        default Stream<NoodleExpression> wrap(BiFunction<Expression<?>,CriteriaBuilder,Optional<Expression<?>>> wrapper, String... fields){
            return getColumns(null, fields).map(c -> (root,query,cb) -> {
                Expression<?> expression = c.apply(root,query,cb).flatMap(ex -> wrapper.apply(ex, cb)).orElse(cb.nullLiteral(Object.class));
                expression.alias(c.getAlias());
                return Optional.of(expression);
            });
        }

        default Stream<NoodleExpression> wrap(BiFunction<Expression<Number>,CriteriaBuilder,Optional<Expression<? extends Number>>> wrapper, String alias,
                                              Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            NoodleExpression wrap = (root, query,criteriaBuilder) -> {
                Expression<? extends Number> expression = Optional
                        .ofNullable(userFunc.apply(new ArithmeticExpression.Builder()))
                        .map(a -> a.getExpresion().apply(root,query, criteriaBuilder))
                        .flatMap(ex -> wrapper.apply(ex, criteriaBuilder))
                        .orElse(criteriaBuilder.nullLiteral(Number.class));
                expression.alias(alias);
                return Optional.of(expression);
            };

            return Stream.of(wrap);
        }

        default Stream<SampleColumn> getColumns(String prefix, String... fields){
            return Stream.of(fields).map(i -> SampleColumn.build(i, prefix)).filter(Objects::nonNull);
        }

        default Stream<SampleColumn> select(String... fields){
            return getColumns(null, fields);
        }

        default Stream<NoodleExpression> coalesce(String alias, Function<CoalesceBuilder,CoalesceExpression> userFunc){
            NoodleExpression wrap = (root, query,criteriaBuilder) -> {
                Expression<?> expression = Optional
                        .ofNullable(userFunc.apply(new CoalesceExpression.Builder()))
                        .flatMap(i -> i.apply(root, query ,criteriaBuilder))
                        .orElse(null);
                if(expression == null){
                    expression = criteriaBuilder.nullLiteral(String.class);
                }
                expression.alias(alias);
                return Optional.of(expression);
            };
            return Stream.of(wrap);
        }

        default Stream<NoodleExpression> custom(String alias, BiFunction<Function<String,Path<?>>,CriteriaBuilder,Expression<?>> userFunc){
            NoodleExpression wrap = (root, query,criteriaBuilder) -> {
                Expression<?> expression = Optional
                        .ofNullable(userFunc.apply((s) -> root.apply(s).orElseThrow(() -> new IllegalArgumentException("field not found " + s)),criteriaBuilder))
                        .orElse(null);
                if(expression == null){
                    expression = criteriaBuilder.literal(0);
                }
                expression.alias(alias);
                return Optional.of(expression);
            };
            return Stream.of(wrap);
        }

        default Stream<NoodleExpression> math(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            return wrap((e, cb) -> Optional.of(e), alias, userFunc);
        }

        default Stream<NoodleExpression> concat(String alias, Function<ConcatExpression,ConcatExpression> userFunc){
            NoodleExpression wrap = (root,query, criteriaBuilder) -> {
                Expression<?> expression = Optional
                        .ofNullable(userFunc.apply(new ConcatExpression()))
                        .flatMap(a -> a.apply(root, query,criteriaBuilder))
                        .orElse(criteriaBuilder.literal(""));
                expression.alias(alias);
                return Optional.of(expression);
            };
            return Stream.of(wrap);
        }

        default Stream<NoodleExpression> count(String... fields){
            return wrap((ex, cb) -> Optional.of(cb.count(ex)), fields);
        }

        default Stream<NoodleExpression> sum(String... fields){
            return wrap((ex, cb) -> {
                if(Number.class.isAssignableFrom(ex.getJavaType())){
                    return Optional.of(cb.sum((Expression<Number>) ex));
                }
                return Optional.empty();
            }, fields);
        }

        default Stream<NoodleExpression> sum(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            return wrap((e, cb) -> Optional.of(cb.sum(e)), alias, userFunc);
        }

        default Stream<NoodleExpression> avg(String... fields){
            return wrap((ex, cb) -> {
                if(Number.class.isAssignableFrom(ex.getJavaType())){
                    return Optional.of(cb.avg((Expression<Number>) ex));
                }
                return Optional.empty();
            }, fields);
        }

        default Stream<NoodleExpression> avg(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            return wrap((e, cb) -> Optional.of(cb.avg(e)), alias, userFunc);

        }

        default Stream<NoodleExpression> length(String... fields){
            return wrap((ex, cb) -> Optional.of(cb.length(ex.as(String.class))), fields);
        }

        default Stream<NoodleExpression> field(String... fields){
            return wrap((ex, cb) -> Optional.of(ex), fields);
        }

        default Stream<NoodleExpression> max(String... fields){
            return wrap((ex, cb) -> {
                if(Number.class.isAssignableFrom(ex.getJavaType())){
                    return Optional.of(cb.max((Expression<Number>) ex));
                }else if(Comparable.class.isAssignableFrom(ex.getJavaType())){
                    return Optional.of(cb.greatest((Expression<Comparable>) ex));
                }
                return Optional.empty();
            }, fields);
        }

        default Stream<NoodleExpression> max(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            return wrap((e, cb) -> Optional.of(cb.max(e)), alias, userFunc);

        }

        default Stream<NoodleExpression> min(String... fields){
            return wrap((ex, cb) -> {
                if(Number.class.isAssignableFrom(ex.getJavaType())){
                    return Optional.of(cb.min((Expression<Number>) ex));
                }else if(Comparable.class.isAssignableFrom(ex.getJavaType())){
                    return Optional.of(cb.least((Expression<Comparable>) ex));
                }
                return Optional.empty();
            }, fields);
        }

        default Stream<NoodleExpression> min(String alias, Function<ArithmeticBuilder,ArithmeticExpression> userFunc){
            return wrap((e, cb) -> Optional.of(cb.min(e)), alias, userFunc);
        }
    }
}
