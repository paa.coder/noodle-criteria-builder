package paa.coder.noodleCriteriaBuilder;

import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface FetchStore {

    Set<String> getManyToOneFetches(Class<?> aClass);

    Set<String> getOneToManyFetches(Class<?> aClass);

    <V> Set<SingularAttribute<? super V,?>> getIds(Class<V> aClass);

    class Sample implements FetchStore{

        private final Map<Class<?>,Set<String>> oneToMany;
        private final Map<Class<?>,Set<String>> manyToOne;
        private final NoodleFactory noodleFactory;


        public Sample(NoodleFactory noodleFactory){
            this.noodleFactory = noodleFactory;
            Metamodel metamodel = noodleFactory.getSession().getMetamodel();
            List<Class<?>> entityClasses = metamodel.getEntities().stream().map(Type::getJavaType).collect(Collectors.toList());

            oneToMany = entityClasses
                    .stream()
                    .collect(Collectors.toUnmodifiableMap(Function.identity(), aClass -> noodleFactory.getEntityColumns(aClass).stream().filter(field -> {
                        if(field.isAnnotationPresent(OneToMany.class)){
                            return FetchType.EAGER == field.getAnnotation(OneToMany.class).fetch();
                        }
                        return false;
                    }).map(Field::getName).collect(Collectors.toUnmodifiableSet())));

            manyToOne = entityClasses
                    .stream()
                    .collect(Collectors.toUnmodifiableMap(Function.identity(), aClass -> noodleFactory.getEntityColumns(aClass).stream().filter(field -> {
                        if(field.isAnnotationPresent(ManyToOne.class)){
                            return FetchType.EAGER == field.getAnnotation(ManyToOne.class).fetch();
                        }
                        return false;
                    }).map(Field::getName).collect(Collectors.toUnmodifiableSet())));
        }


        @Override
        public Set<String> getManyToOneFetches(Class<?> aClass){
            return manyToOne.getOrDefault(aClass, new HashSet<>());
        }

        @Override
        public Set<String> getOneToManyFetches(Class<?> aClass){
            return oneToMany.getOrDefault(aClass, new HashSet<>());
        }

        @Override
        public <V> Set<SingularAttribute<? super V,?>> getIds(Class<V> aClass){

            EntityType<V> entityType = noodleFactory
                    .getSession()
                    .getMetamodel()
                    .getEntities()
                    .stream()
                    .filter(i -> i.getJavaType().equals(aClass))
                    .findFirst()
                    .map(i -> (EntityType<V>) i)
                    .orElseThrow(() -> new NoodleException(String.format("entity of class %s not found", aClass.getName())));

            Set<SingularAttribute<? super V,?>> collect = entityType
                    .getSingularAttributes()
                    .stream()
                    .filter(SingularAttribute::isId)
                    .collect(Collectors.toSet());
            if(collect.isEmpty()){
                throw new NoodleException(String.format("ids in %s not found", aClass.getName()));
            }
            return collect;
        }

        public static class SingletonWrapper {

            private static Sample INSTANCE;

            public static Sample getInstance(NoodleFactory noodleFactory){
                if(INSTANCE == null){
                    INSTANCE = new Sample(noodleFactory);
                }
                return INSTANCE;
            }
        }
    }
}
