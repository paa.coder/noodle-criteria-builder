package paa.coder.noodleCriteriaBuilder.springAdapters;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({NoodleSpringAutoConfigure.class})
public @interface EnableNoodleFilter {
}
