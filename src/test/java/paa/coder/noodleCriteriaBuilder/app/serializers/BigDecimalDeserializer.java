package paa.coder.noodleCriteriaBuilder.app.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.math.BigDecimal;

public class BigDecimalDeserializer extends StdDeserializer<BigDecimal> {

    public BigDecimalDeserializer() {
        this(null);
    }

    public BigDecimalDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public BigDecimal deserialize(JsonParser jsonParser,
                               DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        BigDecimal bigDecimal = new BigDecimal(node.asText().replace(",","."));
        return bigDecimal;
    }
}
