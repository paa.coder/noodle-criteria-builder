package paa.coder.noodleCriteriaBuilder.queryBuilder;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;

import static org.assertj.core.api.Assertions.assertThat;

@DataSet(cleanBefore = true)
class DeleteQueryTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/deleteQueryTest.yml", transactional = true)
    void delete(){

        DeleteQuery<Book> deleteQuery = factory().delete(Book.class);
        assertThat(SQLExtractor.from(deleteQuery.query())).isEqualTo("delete from book");
        Assertions.assertThrows(NoodleException.class, deleteQuery::delete);

        deleteQuery.where(w -> w.where("authorId", 1));
        assertThat(SQLExtractor.from(deleteQuery.query())).isEqualTo("delete from book where author_id=1");
        assertThat(deleteQuery.delete()).isNotNull().isEqualTo(2);
    }

    @Test
    @DataSet(value = "sample/deleteQueryTest.yml", transactional = true)
    void deleteForce(){
        DeleteQuery<Book> deleteQuery = factory().delete(Book.class);
        assertThat(SQLExtractor.from(deleteQuery.query())).isEqualTo("delete from book");
        assertThat(deleteQuery.deleteForce()).isNotNull().isEqualTo(3);
    }

    @Test
    @DataSet(value = "sample/deleteQueryTest.yml", transactional = true)
    void deleteNoodle(){
        assertThat(factory().select(Book.class).stream().count()).isEqualTo(3);
        Book b = factory().find(Book.class,1).orElse(null);
        assertThat(b).isNotNull();
        factory().delete(b);
        assertThat(factory().select(Book.class).stream().count()).isEqualTo(2);
    }

}