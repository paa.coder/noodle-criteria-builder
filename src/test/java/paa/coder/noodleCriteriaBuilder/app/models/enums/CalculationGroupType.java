package paa.coder.noodleCriteriaBuilder.app.models.enums;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;
import java.util.stream.Stream;

@JsonSerialize(using = CalculationGroupType.Serializer.class)
@JsonDeserialize(using = CalculationGroupType.Deserializer.class)
public enum CalculationGroupType{

    house("Жилищная",
          false,
          false,
          "м²",
          2d),
    custom_account("На л/с",
                   false,
                   false,
                   "шт.",
                   4d),
    flat("На помещение",
         false,
         false,
         "шт.",
        3d),
    gas("Газ",
        true,
        false,
        "м³",
        4.5),
    hvs("Холодная вода",
        true,
        false,
        "м³",
        5d);

    private final String translate;
    public final Boolean isCommunal;
    public final Boolean isKrsoi;
    public final String metric;
    public final Double priority;

    CalculationGroupType(String translate, Boolean isCommunal, Boolean isKrsoi, String metric, Double priority){
        this.translate = translate;
        this.isCommunal = isCommunal;
        this.isKrsoi = isKrsoi;
        this.metric = metric;
        this.priority = priority;
    }

    public String getTranslate() {
        return translate;
    }

    public static class Serializer extends JsonSerializer<CalculationGroupType> {
        public Serializer() {
        }

        public void serialize(CalculationGroupType eNum, JsonGenerator generator, SerializerProvider serializerProvider) throws IOException{
            generator.writeStartObject();
            generator.writeFieldName("type");
            generator.writeString(eNum.name());

            generator.writeFieldName("name");
            generator.writeString(eNum.translate);

            generator.writeEndObject();
        }
    }

    public static class Deserializer extends JsonDeserializer<CalculationGroupType> {

        @Override
        public CalculationGroupType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException,
                                                                                                                             JsonProcessingException{
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);
            String enumName;
            if (node instanceof TextNode) {
                enumName = node.textValue();
            } else {
                enumName = node.get("type").asText();
            }

            return Stream.of(CalculationGroupType.values()).filter(i->i.name().equals(enumName)).findFirst().orElse(null);
        }
    }

}