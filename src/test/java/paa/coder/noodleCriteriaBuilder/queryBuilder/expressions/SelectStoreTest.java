package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Author;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataSet(cleanBefore = true)
class SelectStoreTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void select(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.select("info.date"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select book0_.date as col_0_0_ from book book0_");

        assertDoesNotThrow(() -> select.stream(1));

        MultiSelectQuery<Book> selectJoin = factory().select(Book.class).select(s -> s.select("author.name as a_name"));
        assertThat(SQLExtractor.from(selectJoin.query())).isEqualTo(
                "select author1_.name as col_0_0_ from book book0_ cross join author author1_ where book0_.author_id=author1_.id");

        assertThat(selectJoin.where(w -> w.where("id", 3)).stream(1).findFirst())
                .isNotEmpty()
                .get()
                .hasFieldOrPropertyWithValue("a_name", "Frank Herbert");
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void selectWithPrefix(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.selectWithPrefix("info", Book.Info.class));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select book0_.date as col_0_0_, book0_.size as col_1_0_, book0_.comment as col_2_0_ from book book0_");

        assertDoesNotThrow(() -> select.stream(1));
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void coalesce(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.coalesce("info", c -> c.by("info.comment").or("name")));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select coalesce(book0_.comment, book0_.name) as col_0_0_ from book book0_");

        assertThat(select.where(w -> w.where("id", 3)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("info", "Dune");
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void custom(){

        MultiSelectQuery<Book> select = factory()
                .select(Book.class)
                .select(s -> s.custom("length", (pf, cb) -> cb.length(pf.apply("name").as(String.class))));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select length(book0_.name) as col_0_0_ from book book0_");

        assertThat(select.where(w -> w.where("id", 3)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("length", 4);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void math(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.math("math", b -> b.from("info.size").plus(1)));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select book0_.size+1 as col_0_0_ from book book0_");

        assertThat(select.where(w -> w.where("id", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("math", 3);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void concat(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.concat("concat", b -> b.addField("name").add(":", 1)));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select (((''||book0_.name)||':')||'1') as col_0_0_ from book book0_");

        assertThat(select.where(w -> w.where("id", 3)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("concat", "Dune:1");
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void length(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.length("name as length"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select length(book0_.name) as col_0_0_ from book book0_");
        assertThat(select.where(w -> w.where("id", 3)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("length", 4);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void count(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.count("info.size as sizeIs")).group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select count(book0_.size) as col_0_0_ from book book0_ group by book0_.author_id");
        assertThat(select.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 2L);

        MultiSelectQuery<Book> select2 = factory().select(Book.class).select(s -> s.count("info.size as sizeIs")).group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(select2.query())).isEqualTo(
                "select count(book0_.size) as col_0_0_ from book book0_ group by book0_.author_id");
        assertThat(select2.where(w -> w.where("authorId", 2)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 0L);

    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void sum(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.sum("info.size as sizeIs")).group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select sum(book0_.size) as col_0_0_ from book book0_ group by book0_" +
                                                                             ".author_id");

        assertThat(select.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 12L);

        MultiSelectQuery<Book> selectB = factory()
                .select(Book.class)
                .select(s -> s.sum("sizeIs", b -> b.from("info.size").minus(1)))
                .group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(selectB.query())).isEqualTo(
                "select sum(book0_.size-1) as col_0_0_ from book book0_ group by book0_.author_id");

        assertThat(selectB.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 10L);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void avg(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.avg("info.size as sizeIs")).group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select avg(cast(book0_.size as double)) as col_0_0_ from book book0_ group by book0_.author_id");

        assertThat(select.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 6.0);

        MultiSelectQuery<Book> selectB = factory()
                .select(Book.class)
                .select(s -> s.avg("sizeIs", b -> b.from("info.size").minus(1)))
                .group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(selectB.query())).isEqualTo(
                "select avg(cast(book0_.size-1 as double)) as col_0_0_ from book book0_ group by book0_.author_id");

        assertThat(selectB.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 5.0);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void max(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.max("info.size as sizeIs")).group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select max(book0_.size) as col_0_0_ from book book0_ group by book0_" +
                                                                             ".author_id");

        assertThat(select.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 10);

        MultiSelectQuery<Book> selectB = factory()
                .select(Book.class)
                .select(s -> s.max("sizeIs", b -> b.from("info.size").minus(1)))
                .group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(selectB.query())).isEqualTo(
                "select max(book0_.size-1) as col_0_0_ from book book0_ group by book0_.author_id");

        assertThat(selectB.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 9);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void min(){

        MultiSelectQuery<Book> select = factory().select(Book.class).select(s -> s.min("info.size as sizeIs")).group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo("select min(book0_.size) as col_0_0_ from book book0_ group by book0_" +
                                                                             ".author_id");

        assertThat(select.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 2);

        MultiSelectQuery<Book> selectB = factory()
                .select(Book.class)
                .select(s -> s.min("sizeIs", b -> b.from("info.size").minus(1)))
                .group(g -> g.select("authorId"));
        assertThat(SQLExtractor.from(selectB.query())).isEqualTo(
                "select min(book0_.size-1) as col_0_0_ from book book0_ group by book0_.author_id");

        assertThat(selectB.where(w -> w.where("authorId", 1)).stream(1).findFirst()).isNotEmpty().get().hasFieldOrPropertyWithValue("sizeIs", 1);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml")
    void subquery(){

        MultiSelectQuery<Author> select = factory()
                .select(Author.class)
                .select(s -> s
                        .select("name as a_name")
                        .subquery("sample_name", sqb -> sqb.from(Sample.class, "s").where(w -> w.where("s.id", "#id")).select(ss -> ss.select("s.name"))));

        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select author0_.name as col_0_0_, (select sample1_.name from sample sample1_ where sample1_.id=author0_.id) as col_1_0_ from " +
                        "author author0_");

        assertThat(select.where(w -> w.where("id", 1)).stream(1).findFirst()).isNotEmpty().get()
                .hasFieldOrPropertyWithValue("a_name", "Philip K. Dick")
                .hasFieldOrPropertyWithValue("sample_name", "first");
    }


}