package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.queryBuilder.UpdateQuery;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataSet(cleanBefore = true)
class SetterBuilderTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/selectStoreTest.yml", transactional = true)
    void custom(){

        UpdateQuery<Book> update = factory()
                .update(Book.class)
                .set(s -> s.custom("info.size", (pf, cb) -> cb.length(pf.apply("name").as(String.class))));
        assertThat(SQLExtractor.from(update.query())).isEqualTo("update book set size=length(name)");
        assertDoesNotThrow(update::updateForce);
        assertThat(update.where(w -> w.where("id", 3)).update()).isEqualTo(1);
        assertThat(factory().find(Book.class, 3)).isNotEmpty().get().extracting(i -> i.getInfo().getSize()).isEqualTo(4);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml", transactional = true)
    void math(){
        UpdateQuery<Book> update = factory().update(Book.class).set(s -> s.math("info.size", b -> b.from("id").plus(5)));
        assertThat(SQLExtractor.from(update.query())).isEqualTo("update book set size=id+5");
        assertThat(update.where(w -> w.where("id", 1)).update()).isEqualTo(1);
        assertThat(factory().find(Book.class, 1)).isNotEmpty().get().extracting(i -> i.getInfo().getSize()).isEqualTo(6);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml", transactional = true)
    void subquery(){
        UpdateQuery<Book> update = factory()
                .update(Book.class)
                .set(s -> s.subquery("info.size",
                                     b -> b.from(Sample.class, "s").select(_s -> _s.length("s.name"))));

        assertThat(SQLExtractor.from(update.query())).isEqualTo("update book set size=cast((select length(sample1_.name) from sample sample1_) as integer)");
        assertThat(update.where(w -> w.where("id", 1)).update()).isEqualTo(1);
        assertThat(factory().find(Book.class, 1)).isNotEmpty().get().extracting(i -> i.getInfo().getSize()).isEqualTo(5);
    }

    @Test
    @DataSet(value = "sample/selectStoreTest.yml", transactional = true)
    void set(){
        UpdateQuery<Book> update = factory().update(Book.class).set(s -> s.set("info.size", 1));
        assertThat(SQLExtractor.from(update.query())).isEqualTo("update book set size=1");
        assertThat(update.where(w -> w.where("id", 1)).update()).isEqualTo(1);
        assertThat(factory().find(Book.class, 1)).isNotEmpty().get().extracting(i -> i.getInfo().getSize()).isEqualTo(1);

        UpdateQuery<Book> updateNull = factory().update(Book.class).set(s -> s.set("info.size", null));
        assertThat(SQLExtractor.from(updateNull.query())).isEqualTo("update book set size=null");
        assertThat(updateNull.where(w -> w.where("id", 1)).update()).isEqualTo(1);
        assertThat(factory().find(Book.class, 1)).isNotEmpty().get().extracting(i -> i.getInfo().getSize()).isNull();

    }


}