package paa.coder.noodleCriteriaBuilder.interfaces;

import javax.persistence.criteria.Path;
import java.util.Optional;
import java.util.function.Function;

public interface PathFinder extends Function<String,Optional<Path<?>>> {
}
