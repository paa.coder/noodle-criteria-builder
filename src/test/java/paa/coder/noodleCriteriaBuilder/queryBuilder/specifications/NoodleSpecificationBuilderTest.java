package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class NoodleSpecificationBuilderTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void or(){

        MultiSelectQuery<Book> where = factory()
                .select(Book.class)
                .select(s -> s.select("id"))
                .where(w -> w.where("name", "#info.comment").or("name", "Dune"));
        assertThat(SQLExtractor.from(where.query())).isEqualTo(
                "select book0_.id as col_0_0_ from book book0_ where book0_.name=book0_.comment or book0_.name=?");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(2);


        MultiSelectQuery<Book> where2 = factory()
                .select(Book.class)
                .select(s -> s.select("id"))
                .where(w -> w
                        .where("name", "#info.comment")
                        .or("name", "Dune")
                        .and("info.size", ">", 0)
                        .orIsNul("info.date")
                        .andNot("id", "in", Set.of(1, 2)));
        assertThat(SQLExtractor.from(where2.query())).isEqualTo(
                "select book0_.id as col_0_0_ from book book0_ where ((book0_.name=book0_.comment or book0_.name=?) and book0_.size>0 or book0_.date is null) and (book0_.id not in  (1 , 2))");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where2::stream);

    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void orWrap(){

        MultiSelectQuery<Book> where = factory()
                .select(Book.class)
                .select(s -> s.select("id"))
                .where(w -> w.where("name", "#info.comment").or(_w -> _w.where("name", "Dune").and("authorId", 2)));
        assertThat(SQLExtractor.from(where.query())).isEqualTo(
                "select book0_.id as col_0_0_ from book book0_ where book0_.name=book0_.comment or book0_.name=? and book0_.author_id=2");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(2);

        MultiSelectQuery<Book> whereNot = factory()
                .select(Book.class)
                .select(s -> s.select("id"))
                .where(w -> w.where("name", "#info.comment").orNot(_w -> _w.where("name", "Dune").and("authorId", 2)));
        assertThat(SQLExtractor.from(whereNot.query())).isEqualTo(
                "select book0_.id as col_0_0_ from book book0_ where book0_.name=book0_.comment or book0_.name<>? or book0_.author_id<>2");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) whereNot::stream);

        MultiSelectQuery<Book> where2 = factory()
                .select(Book.class)
                .select(s -> s.select("id"))
                .where(w -> w.where("name", "#info.comment").and(_w -> _w.where("name", "Dune").or("authorId", 2)));
        assertThat(SQLExtractor.from(where2.query())).isEqualTo(
                "select book0_.id as col_0_0_ from book book0_ where book0_.name=book0_.comment and (book0_.name=? or book0_.author_id=2)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where2::stream);

        MultiSelectQuery<Book> where2Not = factory()
                .select(Book.class)
                .select(s -> s.select("id"))
                .where(w -> w.where("name", "#info.comment").andNot(_w -> _w.where("name", "Dune").or("authorId", 2)));
        assertThat(SQLExtractor.from(where2Not.query())).isEqualTo(
                "select book0_.id as col_0_0_ from book book0_ where book0_.name=book0_.comment and book0_.name<>? and book0_.author_id<>2");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where2Not::stream);
    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void withSelections(){

        MultiSelectQuery<Book> where = factory()
                .select(Book.class)
                .select(s -> s.select("id"))
                .where(w -> w
                        .where(c -> c.math("m", m -> m.from("id").multiply(10)), "#info.size")
                        .or(_w -> _w.where("name", "Dune").and("authorId", 2)));
        assertThat(SQLExtractor.from(where.query())).isEqualTo(
                "select book0_.id as col_0_0_ from book book0_ where book0_.id*10=book0_.size or book0_.name=? and book0_.author_id=2");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);
    }
}