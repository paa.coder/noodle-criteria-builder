package paa.coder.noodleCriteriaBuilder.queryBuilder.expressions;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class OrderBuilderTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    void order(){
        MultiSelectQuery<Sample> select = factory()
                .select(Sample.class)
                .select(s -> s.select("id")).order(b->b.asc("rate","cost").desc("volume").desc("id"));
        assertThat(SQLExtractor.from(select.query())).isEqualTo(
                "select sample0_.id as col_0_0_ from sample sample0_ order by sample0_.rate asc, sample0_.cost asc, sample0_.volume desc, sample0_.id desc");
        assertDoesNotThrow(()->select.stream(1));

    }

    @Test
    void orderBuilder(){
        MultiSelectQuery<Sample> coalesce = factory()
                .select(Sample.class)
                .select(s -> s.select("id"))
                .order(b->b.asc(_b->_b.coalesce(c->c.by("volume").or("cost"))).desc(_b->_b.coalesce(c->c.by("rate").or("id"))));
        assertThat(SQLExtractor.from(coalesce.query())).isEqualTo(
                "select sample0_.id as col_0_0_ from sample sample0_ order by coalesce(sample0_.volume, sample0_.cost) asc, coalesce(sample0_.rate, sample0_.id) desc");

        assertDoesNotThrow(()->coalesce.stream(1));

        MultiSelectQuery<Sample> math = factory()
                .select(Sample.class)
                .select(s -> s.select("id"))
                .order(b->b.asc(_b->_b.math(c->c.from(1).multiply("cost"))).desc(_b->_b.math(c->c.from(1).divide("rate"))));
        assertThat(SQLExtractor.from(math.query())).isEqualTo(
                "select sample0_.id as col_0_0_ from sample sample0_ order by 1*sample0_.cost asc, 1/sample0_.rate desc");
        assertDoesNotThrow(()->math.stream(1));

        MultiSelectQuery<Sample> concat = factory()
                .select(Sample.class)
                .select(s -> s.select("id"))
                .order(b->b.asc(_b->_b.concat(c->c.addField("name").add("-").addField("comment"))).desc(_b->_b.concat(c->c.addField("name").add("-").addField("comment"))));
        assertThat(SQLExtractor.from(concat.query())).isEqualTo(
                "select sample0_.id as col_0_0_ from sample sample0_ order by (((?||sample0_.name)||?)||sample0_.comment) asc, (((?||sample0_.name)||?)||sample0_.comment) desc");
        assertDoesNotThrow(()->concat.stream(1));

        MultiSelectQuery<Sample> custom = factory()
                .select(Sample.class)
                .select(s -> s.select("id"))
                .order(b->b.asc(_b->_b.custom((pf,cb)->cb.length(pf.apply("name").as(String.class)))).desc(_b->_b.custom((pf,cb)->cb.length(pf.apply("comment").as(String.class)))));
        assertThat(SQLExtractor.from(custom.query())).isEqualTo(
                "select sample0_.id as col_0_0_ from sample sample0_ order by length(sample0_.name) asc, length(sample0_.comment) desc");
        assertDoesNotThrow(()->custom.stream(1));

        MultiSelectQuery<Sample> length = factory()
                .select(Sample.class)
                .select(s -> s.select("id"))
                .order(b->b.asc(_b->_b.length("name")).desc(_b->_b.length("comment")));
        assertThat(SQLExtractor.from(length.query())).isEqualTo(
                "select sample0_.id as col_0_0_ from sample sample0_ order by length(sample0_.name) asc, length(sample0_.comment) desc");
        assertDoesNotThrow(()->length.stream(1));

    }
}