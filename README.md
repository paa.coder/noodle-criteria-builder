# Hibernate Specification Search and Spring Filter

## Installation

```xml

<dependency>
    <groupId>io.gitlab.paa.coder</groupId>
    <artifactId>noodle-criteria-builder</artifactId>
    <version>2.1.0</version>
</dependency>
```

```java
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Service;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;

@Service
public class AppNoodleFactory implements NoodleFactory {


    private final ObjectMapper objectMapper;
    private final LocalSessionFactoryBean sessionFactory;

    public AppNoodleFactory(ObjectMapper objectMapper, LocalSessionFactoryBean sessionFactory){
        this.objectMapper = objectMapper;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public SessionFactory sessionFactory(){
        return sessionFactory.getObject();
    }

    @Override
    public ObjectMapper getObjectMapper(){
        return objectMapper.copy().registerModule(new Hibernate5Module());
    }

}
```

```java

@SpringBootApplication
@EnableNoodleFilter
public class Application {
    public static void main(String[] args){
        SpringApplication.run(CountingApplication.class, args);
    }
}
```

## Usages

### a. filter

```java
@NoodleMapping("/") 
@Transactional(readOnly = true) 
public RestPage<Book> find(@NoodleFilter NoodleRestFilter filter){
    return filter.build(Book.class);
}
```

### b. filter predicate handler

```java
@NoodleMapping("/") 
@Transactional(readOnly = true) 
public RestPage<Book> find(@NoodleFilter NoodleRestFilter filter){
    filter.withHandler("sizeOfName",
    (rc,b)->b.where(c->c.length("name"),rc.getOperator(),rc.getValue()));
    return filter.build(Book.class);
}
```

### c. filter order handler

```java
@NoodleMapping("/") 
@Transactional(readOnly = true) 
public RestPage<Book> find(@NoodleFilter NoodleRestFilter filter){
    filter.withOrderHandler("sizeOfName",eb->eb.length("name"));
    return filter.build(Book.class);
}
```

### d. Selection

```java
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;

@Autowired public NoodleFactory factory;

@Transactional 
public Stream<Book> find(){
    return factory.select(Book.class).where(w->w.where("name","<>","#info.comment").orIsNul("info.comment")).stream(10,1);
}
@Transactional 
public Stream<NoodleResult> find(){
    return factory.select(Book.class).select(s->s.count("id as countId")).group(g->g.select("authorId"),true).having(h->h.where("authorId",2)).stream();
}

```

### e. Update

```java
@Autowired 
public NoodleFactory factory;
@Transactional 
public Integer update(){
    return factory().update(Book.class).set(s->s.set("info.size",1)).where(w -> w.where("info.size", "is null",(Object) null)).update();
}

```

### f. Delete

```java
@Autowired 
public NoodleFactory factory;
@Transactional 
public Integer delete(){
    return factory().delete(Book.class).where(w -> w.where("info.size", "is null",(Object) null)).delete();
}

```

### g. filter criteria field converter
#### exist policies
```java
interface FieldNamePolicy extends Function<String,String>{

    String DEFAULT_VALUE = "DEFAULT";
    String CAMEL_CASE_VALUE = "CAMEL_CASE";
    String SNAKE_CASE_VALUE = "SNAKE_CASE";
    String LOWER_SNAKE_CASE_VALUE = "LOWER_SNAKE_CASE";
    String UPPER_SNAKE_CASE_VALUE = "UPPER_SNAKE_CASE";
}
```
#### setting default policy

```java
class FilterRequestMapper implements  NoodleRequestMapper {
   
    @Override
    public String getDefaultFieldNamePolicy(){
        return RestCriteriaConverter.FieldNamePolicy.SNAKE_CASE_VALUE;
    }
}
```

#### specify a policy for a specific filter

```java
@NoodleMapping("/") 
@Transactional(readOnly = true) 
public RestPage<Book> find(@NoodleFilter(criteriaFieldNamePolicy=RestCriteriaConverter.FieldNamePolicy.CAMEL_CASE_VALUE) NoodleRestFilter filter){
    return filter.build(Book.class);
}
```
#### custom policy

```java
class FilterRequestMapper implements  NoodleRequestMapper {

    @Override
    public String getDefaultFieldNamePolicy(){
        return "customPolicy";
    }
    @Override
    public void customizeRestCriteriaConverterStore(RestCriteriaConverter.Store store){
        store.addFieldNamePolicy("customPolicy",s->s.toUpperCase());
    }
}

@NoodleMapping("/")
@Transactional(readOnly = true)
public RestPage<Book> find(@NoodleFilter(criteriaFieldNamePolicy="customPolicy") NoodleRestFilter filter){
    return filter.build(Book.class);
}
```