package paa.coder.noodleCriteriaBuilder.springAdapters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;

import java.util.List;

@Configuration
@ConditionalOnBean({NoodleFactory.class})
public class NoodleSpringAutoConfigure implements WebMvcConfigurer{

    @Autowired
    private NoodleFactory noodleFactory;
    @Autowired(required = false)
    private NoodleRequestMapper noodleRequestMapper;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers){
        if(noodleRequestMapper==null){
            noodleRequestMapper=new NoodleRequestMapper() {
                @Override
                public NoodleFactory noodleFactory(){
                    return noodleFactory;
                }
            };
        }
        resolvers.add(new NoodleArgumentResolver(noodleRequestMapper));
    }

    @Bean
    public NoodleMappingFilter noodleMappingFilter(RequestMappingHandlerMapping reqMap){
        return new NoodleMappingFilter(reqMap);
    }
}
