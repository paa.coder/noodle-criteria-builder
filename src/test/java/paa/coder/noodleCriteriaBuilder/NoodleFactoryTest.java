package paa.coder.noodleCriteriaBuilder;

import lombok.Data;
import org.junit.jupiter.api.Test;

import javax.persistence.Transient;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class NoodleFactoryTest extends DbContext{

    @Test
    void getEntityColumnNames(){
        Set<String> strings = factory().getEntityColumnNames(TestN.class);
        assertThat(strings).isNotNull().isNotEmpty().hasSize(2).doesNotContain("scale");
    }

    @Data
    public static class TestN{
        private BigDecimal rate;
        private Instant createdAt;
        @Transient
        private Integer scale;
    }

}