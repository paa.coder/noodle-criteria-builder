package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SubqueryBuilder;

import javax.persistence.criteria.CommonAbstractCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;

public class LiteralSubquerySpecification<T> extends SubquerySpecification<T>{

    private final Object object;

    public LiteralSubquerySpecification(Object object, String operator, SubqueryBuilder<T> queryBuilder){
        this(object, operator,queryBuilder,false);
    }
    public LiteralSubquerySpecification(Object object, String operator, SubqueryBuilder<T> queryBuilder, Boolean isNot){
        super(queryBuilder,operator, isNot);
        this.object = object;

    }

    @Override
    protected Expression<?> getExpression(PathFinder pathFinder, CommonAbstractCriteria criteriaQuery, CriteriaBuilder criteriaBuilder){
        return criteriaBuilder.literal(object);
    }
}
