package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SampleColumn;

import javax.persistence.criteria.*;
import java.util.Optional;

public class FieldToFieldSpecification extends NoodleSpecification {

    private final NoodleExpression left;
    private final NoodleExpression right;

    public FieldToFieldSpecification(String left, String operator, String right, Boolean isNot){
        this( SampleColumn.build(left, null), operator, right, isNot);
    }

    public FieldToFieldSpecification(NoodleExpression left, String operator, String right, Boolean isNot){
        super(operator, isNot);
        this.left = left;
        this.right = SampleColumn.build(right, null);
    }

    private Predicate toStringPredicate(Expression<String> left, Expression<String> right, CriteriaBuilder b){
        return toStringExpression(left, right, b);
    }

    private <X extends Comparable<X>> Predicate toComparablePredicate(Expression<X> left,Expression<X> right,CriteriaBuilder b){
        return toComparableExpression(left, right, b);
    }
    private <X> Predicate toEqualsPredicate(Expression<X> left,Expression<?> right,CriteriaBuilder b) {
        return toEqualsExpression(left,right,b);
    }

    @Override
    public Predicate apply(PathFinder pathFinder, CommonAbstractCriteria query, CriteriaBuilder criteriaBuilder){

        Predicate predicate = Optional
                .ofNullable(left)
                .flatMap(i->i.apply(pathFinder,query,criteriaBuilder))
                .flatMap(left -> Optional
                        .ofNullable(right)
                        .flatMap(_i->_i.apply(pathFinder,query,criteriaBuilder))
                        .filter(right->left.getJavaType().isAssignableFrom(right.getJavaType()))
                        .map(right -> {
                            if(String.class.isAssignableFrom(left.getJavaType())){
                                return toStringPredicate((Expression<String>) left, (Expression<String>) right, criteriaBuilder);
                            }else if(Comparable.class.isAssignableFrom(left.getJavaType())){
                                return toComparablePredicate((Expression<Comparable>)left,(Expression<Comparable>)right, criteriaBuilder);
                            }else{
                                return toEqualsPredicate(left,right, criteriaBuilder);
                            }
                        }))
                .orElse(null);

        return predicate == null ? null : isNot ? predicate.not() : predicate;
    }
}
