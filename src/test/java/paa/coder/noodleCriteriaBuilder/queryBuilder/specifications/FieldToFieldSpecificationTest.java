package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataSet(cleanBefore = true)
class FieldToFieldSpecificationTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toEquals(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("name", "#info.comment"));

        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name=book0_.comment");

        assertThat(where.stream().collect(Collectors.toList())).hasSize(1).first().hasFieldOrPropertyWithValue("id",2);
    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toNotEquals(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("name", "<>","#info.comment").orIsNul("info.comment"));

        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name<>book0_.comment or book0_.comment is null");

        assertThat(where.stream().collect(Collectors.toList())).hasSize(2);

        MultiSelectQuery<Book> where1 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("name", "!=","#info.comment").orIsNul("info.comment"));

        assertThat(SQLExtractor.from(where1.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name<>book0_.comment or book0_.comment is null");

        assertThat(where1.stream().collect(Collectors.toList())).hasSize(2);
    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toComparing(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("id", "<","#info.size"));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.id<book0_.size");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);

        MultiSelectQuery<Book> where2 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("id", "<=","#info.size"));
        assertThat(SQLExtractor.from(where2.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.id<=book0_.size");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where2::stream);

        MultiSelectQuery<Book> where3 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("id", ">=","#info.size"));
        assertThat(SQLExtractor.from(where3.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.id>=book0_.size");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where3::stream);

        MultiSelectQuery<Book> where4 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("id", ">","#info.size"));
        assertThat(SQLExtractor.from(where4.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.id>book0_.size");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where4::stream);

    }

}