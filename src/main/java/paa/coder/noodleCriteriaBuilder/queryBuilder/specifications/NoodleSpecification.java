package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import paa.coder.noodleCriteriaBuilder.exceptions.ComparisonOperatorsNotSupport;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodlePredicate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.Set;

public abstract class NoodleSpecification implements NoodlePredicate {

    protected final String operator;
    protected final Boolean isNot;

    public NoodleSpecification(String operator, Boolean isNot){
        this.operator = operator;
        this.isNot = isNot;
    }

    protected boolean isIterable(){
        return Set.of("IN", "NOT IN").contains(operator.toUpperCase().trim());
    }

    public String getOperator(){
        return operator.trim().toUpperCase();
    }

    protected <T> Predicate toEqualsObject(Expression<T> path, T value, CriteriaBuilder b){
        switch(operator.toUpperCase().trim()){
            case "=":{
                return b.equal(path,value);
            }
            case "<>":{
                return b.notEqual(path,value);
            }
            case "!=":{
                return b.notEqual(path,value);
            }
            default:{
                throw new ComparisonOperatorsNotSupport(String.format("operator %s not support", operator));
            }
        }
    }

    protected <T> Predicate toEqualsExpression(Expression<T> path, Expression<?> value, CriteriaBuilder b){
        switch(operator.toUpperCase().trim()){
            case "=":{
                return b.equal(path,value);
            }
            case "<>":{
                return b.notEqual(path,value);
            }
            case "!=":{
                return b.notEqual(path,value);
            }
            default:{
                throw new ComparisonOperatorsNotSupport(String.format("operator %s not support", operator));
            }
        }
    }

    protected <X extends Comparable<X>>Predicate toComparableExpression(Expression<X> path, Expression<X> value, CriteriaBuilder b){
        switch(getOperator()){
            case ">":{
                return b.greaterThan(path,value);
            }
            case ">=":{
                return b.greaterThanOrEqualTo(path,value);
            }
            case "<":{
                return b.lessThan(path,value);
            }
            case "<=":{
                return b.lessThanOrEqualTo(path,value);
            }
            default: return toEqualsExpression(path,value,b);
        }
    }

    protected <X extends Comparable<X>>Predicate toComparableObject(Expression<X> path, X value, CriteriaBuilder b){
        switch(getOperator()){
            case ">":{
                return b.greaterThan(path,value);
            }
            case ">=":{
                return b.greaterThanOrEqualTo(path,value);
            }
            case "<":{
                return b.lessThan(path,value);
            }
            case "<=":{
                return b.lessThanOrEqualTo(path,value);
            }
            default: return toEqualsObject(path,value,b);
        }
    }


    protected Predicate toStringExpression(Expression<String> path, Expression<String> value, CriteriaBuilder b){
        if("LIKE".equals(getOperator())){
            return b.like(path, value);
        }
        if("ILIKE".equals(getOperator())){
            return b.like(b.lower(path), b.lower(value));
        }
        return toComparableExpression(path, value, b);
    }

    protected Predicate toStringObject(Expression<String> path, String value, CriteriaBuilder b){
        if("LIKE".equals(getOperator())){
            return b.like(path, value);
        }
        if("ILIKE".equals(getOperator())){
            return b.like(b.lower(path), value.toLowerCase());
        }
        return toComparableObject(path, value, b);
    }

    protected Predicate toPredicateInNull(Expression<?> path, CriteriaBuilder b){
        switch(operator.toUpperCase().trim()){
            case "IS NULL":{
                return b.isNull(path);
            }
            case "IS NOT NULL":{
                return b.isNotNull(path);
            }
        }
        throw new ComparisonOperatorsNotSupport(String.format("operator %s not support with null value", operator));
    }
}
