package paa.coder.noodleCriteriaBuilder.restFilter;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Author;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.app.models.Order;
import paa.coder.noodleCriteriaBuilder.app.models.Sample;
import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleQuery;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SampleQuery;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestFilter;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestOrder;
import paa.coder.noodleCriteriaBuilder.restFilter.payloads.RestPage;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataSet(cleanBefore = true)
class NoodleRestFilterTest extends DbContext {

    private ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();


    private NoodleRestFilter noodleRestFilter(RestFilter restFilter){
        return new NoodleRestFilter(restFilter, factory());
    }

    @Test
    @DataSet(value = "sample/filterTest.yml")
    public void allFind(){
        NoodleRestFilter restFilter = noodleRestFilter(null);
        assertThat(SQLExtractor.from(restFilter.query(Book.class))).endsWith("from book book0_ inner join author author1_ on book0_.author_id=author1_.id");


        RestPage<Book> page = restFilter.build(Book.class);
        assertThat(page).isNotNull().hasFieldOrPropertyWithValue("totalElements", 3L);
        assertThat(page.getContent()).isNotEmpty().hasSize(3);
        assertThat(page.getFilter()).isNotNull().hasFieldOrPropertyWithValue("page", 0);
        assertThat(page.getFilter()).isNotNull().hasFieldOrPropertyWithValue("pageCount", 500);
        assertThat(page.getFilter().getOrder()).isNotNull().isEmpty();
        assertThat(page.getFilter().getQuery()).isNotNull().hasFieldOrPropertyWithValue("operator", Predicate.BooleanOperator.AND);
        assertThat(page.getFilter().getQuery().getCriteria()).isNotNull().isEmpty();

    }

    @Test
    @DataSet(value = "sample/filterTest.yml")
    @Disabled
    public void allGraphFind(){
        NoodleRestFilter restFilter = noodleRestFilter(null);


        RestPage<Author> page = restFilter.build(Author.class,i->i.withGraph("authorWithBooks"));
        assertThat(page).isNotNull().hasFieldOrPropertyWithValue("totalElements", 3L);
        assertThat(page.getContent()).isNotEmpty().hasSize(3);
        assertThat(page.getFilter()).isNotNull().hasFieldOrPropertyWithValue("page", 0);
        assertThat(page.getFilter()).isNotNull().hasFieldOrPropertyWithValue("pageCount", 500);
        assertThat(page.getFilter().getOrder()).isNotNull().isEmpty();
        assertThat(page.getFilter().getQuery()).isNotNull().hasFieldOrPropertyWithValue("operator", Predicate.BooleanOperator.AND);
        assertThat(page.getFilter().getQuery().getCriteria()).isNotNull().isEmpty();

    }

    @Test
    @DataSet(value = "sample/filterTest.yml")
    public void allFindProjection(){
        NoodleRestFilter restFilter = noodleRestFilter(null);
        Function<SampleQuery<Book>,NoodleQuery<?,NoodleResult>> uFunc = i -> i.select(s -> s.select("author"));
        assertThat(SQLExtractor.from(restFilter.query(Book.class, uFunc)))
                .isEqualTo(
                        "select author1_.id as id1_0_, author1_.name as name2_0_ from book book0_ inner join author author1_ on book0_" +
                                ".author_id=author1_.id");


        RestPage<NoodleResult> page = restFilter.build(Book.class, uFunc);
        assertThat(page).isNotNull().hasFieldOrPropertyWithValue("totalElements", 3L);
        assertThat(page.getContent()).isNotEmpty().hasSize(3);
        assertThat(page.getFilter()).isNotNull().hasFieldOrPropertyWithValue("page", 0);
        assertThat(page.getFilter()).isNotNull().hasFieldOrPropertyWithValue("pageCount", 500);
        assertThat(page.getFilter().getOrder()).isNotNull().isEmpty();
        assertThat(page.getFilter().getQuery()).isNotNull().hasFieldOrPropertyWithValue("operator", Predicate.BooleanOperator.AND);
        assertThat(page.getFilter().getQuery().getCriteria()).isNotNull().isEmpty();

    }


    @Test
    @DataSet(value = "sample/filterTest.yml")
    public void secureFields(){

        NoodleRestFilter restFilter = noodleRestFilter(null);
        restFilter.getRestFilter().where(b -> b.and("info.comment", "cdva"));
        restFilter.disableFields("info");
        Assertions.assertThrows(NoodleException.FieldBlocked.class, () -> restFilter.build(Book.class));
        restFilter.enableFields("info.comment");
        assertThat(SQLExtractor.from(restFilter.query(Book.class))).endsWith("where book0_.comment=?");
        assertDoesNotThrow(() -> restFilter.build(Book.class));
    }

    @Test
    @DataSet(value = "sample/filterTest.yml")
    public void reservedClassName(){

        NoodleRestFilter restFilter = noodleRestFilter(null);
        restFilter.getRestFilter().where(b -> b.and("id", 1));
        assertThat(SQLExtractor.from(restFilter.query(Order.class))).endsWith("from orders order0_ where order0_.id=1");
        assertDoesNotThrow(() -> restFilter.build(Order.class));
    }

    @Test
    @DataSet(value = "sample/filterTest.yml")
    public void wrapper(){

        Consumer<NoodleRestFilter> wrapper = rf -> rf.withHandler("sizeOfName",
                                                                  (rc, b) -> b.where(c -> c.length("name"), rc.getOperator(), rc.getValue()));

        NoodleRestFilter restFilter = noodleRestFilter(null);
        restFilter.getRestFilter().where(b -> b.and("id", ">", 0).and("sizeOfName", 4));
        wrapper.accept(restFilter);
        assertThat(SQLExtractor.from(restFilter.query(Book.class))).endsWith("where book0_.id>0 and length(book0_.name)=4");
        assertDoesNotThrow(() -> restFilter.build(Book.class));

        NoodleRestFilter rf2 = noodleRestFilter(null);
        rf2.getRestFilter().where(b -> b.and("id", ">", 0).or("sizeOfName", "<", 4));
        wrapper.accept(rf2);
        assertThat(SQLExtractor.from(rf2.query(Book.class))).endsWith("where book0_.id>0 or length(book0_.name)<4");
        assertDoesNotThrow(() -> rf2.build(Book.class));

        NoodleRestFilter rf3 = noodleRestFilter(null);
        rf3.getRestFilter().where(b -> b.and("id", ">", 0).or("sizeOfName", "<", 4));
        rf3.withHandler("sizeOfName", null);
        assertThat(SQLExtractor.from(rf3.query(Book.class))).endsWith("where book0_.id>0");
        assertDoesNotThrow(() -> rf3.build(Book.class));

    }

    @Test
    @DataSet(value = "sample/filterTest.yml")
    public void predicates(){
        NoodleRestFilter restFilter = noodleRestFilter(null);
        restFilter.and(b -> b.where("id", ">", 0).and(c -> c.length("name"), 4));
        assertThat(SQLExtractor.from(restFilter.query(Book.class))).endsWith("where book0_.id>0 and length(book0_.name)=4");
        assertDoesNotThrow(() -> restFilter.build(Book.class));

    }

    @Test
    @DataSet(value = "sample/filterTest.yml")
    public void orderFields(){

        Function<RestOrder,NoodleRestFilter> rf = (ro) -> {
            NoodleRestFilter _rf = noodleRestFilter(null);
            _rf.getRestFilter().withPage(0).withPageCount(2).addOrder(ro);
            return _rf;
        };
        NoodleRestFilter rf1 = rf.apply(new RestOrder(false, "name", "id"));
        assertThat(SQLExtractor.from(rf1.query(Book.class))).endsWith("order by book0_.name desc, book0_.id desc");
        assertDoesNotThrow(() -> rf1.build(Book.class));

        NoodleRestFilter rf2 = rf.apply(new RestOrder(false, "sizeOfName", "id"));
        rf2.withOrderHandler("sizeOfName", eb -> eb.length("name"));
        assertThat(SQLExtractor.from(rf2.query(Book.class))).endsWith("order by length(book0_.name) desc, book0_.id desc");
        assertDoesNotThrow(() -> rf2.build(Book.class));

        NoodleRestFilter rf3 = rf.apply(new RestOrder(false, "sizeOfName", "id"));
        rf3.withOrderHandler("sizeOfName", null);
        assertThat(SQLExtractor.from(rf3.query(Book.class))).endsWith("order by book0_.id desc");
        assertDoesNotThrow(() -> rf3.build(Book.class));


    }

    @Test
    @DataSet(value = "sample/naturalOrder.yml")
    public void orderHandler(){


        NoodleRestFilter rf = noodleRestFilter(null);
        rf.addOrderHandler("natural-order-id",eb -> {
            eb.length("id");
            eb.field("id");
            return eb.build();
        });
        rf.getRestFilter().withPage(0).withPageCount(100).addOrder(new RestOrder("natural-order-id")).addOrder(new RestOrder(false,"name"));


        assertThat(SQLExtractor.from(rf.query(Sample.class))).endsWith("order by length(cast(sample0_.id as varchar(255))) asc, sample0_.id asc, sample0_.name desc");
        final List<Integer> content = rf.build(Sample.class).map(i -> i.getId()).getContent();
        assertThat(content).hasSize(6);
        assertThat(content.get(0)).isEqualTo(1);
        assertThat(content.get(1)).isEqualTo(2);
        assertThat(content.get(2)).isEqualTo(10);
        assertThat(content.get(3)).isEqualTo(11);
        assertThat(content.get(4)).isEqualTo(100);
        assertThat(content.get(5)).isEqualTo(110);


    }

}