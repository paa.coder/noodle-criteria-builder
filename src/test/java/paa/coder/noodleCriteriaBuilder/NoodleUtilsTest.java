package paa.coder.noodleCriteriaBuilder;

import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitPlatform.class)
class NoodleUtilsTest {

    @Test
    public void shiftTests(){
        String init = "key.volume.type";
        assertThat(new NoodleUtils.StrShift(init).getLeft()).isPresent().get().isEqualTo("key");
        assertThat(new NoodleUtils.StrShift(init).getRight()).isPresent().get().isEqualTo("volume.type");
    }
}