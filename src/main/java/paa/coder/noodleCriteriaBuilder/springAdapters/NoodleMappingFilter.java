package paa.coder.noodleCriteriaBuilder.springAdapters;

import org.apache.commons.io.IOUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleMapping;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class NoodleMappingFilter extends OncePerRequestFilter {

    private final RequestMappingHandlerMapping reqMap;

    public NoodleMappingFilter(RequestMappingHandlerMapping reqMap){this.reqMap = reqMap;}

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws
                                                                                                                                             ServletException,
                                                                                                                                             IOException{
        filterChain.doFilter(new CacheHttpServletRequest(httpServletRequest),httpServletResponse);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {

        HandlerMethod method = null;
        try{
            method = (HandlerMethod) reqMap.getHandler(request).getHandler();
        }catch(Exception e){
           return true;
        }

        return !method.getMethod().isAnnotationPresent(NoodleMapping.class);
    }

    public static class CacheHttpServletRequest extends HttpServletRequestWrapper {
        private  byte[] body;

        public CacheHttpServletRequest(HttpServletRequest request) {
            super(request);


        }

        @Override
        public ServletInputStream getInputStream() throws IOException {
            if (body == null)
                body=IOUtils.toByteArray(super.getInputStream());

            return new CachedServletInputStream();
        }

        @Override
        public BufferedReader getReader() throws IOException{
            return new BufferedReader(new InputStreamReader(getInputStream()));
        }


        public class CachedServletInputStream extends ServletInputStream {
            private ByteArrayInputStream input;
            public CachedServletInputStream() {
                input = new ByteArrayInputStream(body);
            }

            @Override
            public int read() throws IOException {
                return input.read();
            }

            @Override
            public boolean isFinished(){
                return input.available() == 0;
            }

            @Override
            public boolean isReady(){
                return true;
            }

            @Override
            public void setReadListener(ReadListener readListener){
                try{
                    readListener.onAllDataRead();
                }catch(IOException e){
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
