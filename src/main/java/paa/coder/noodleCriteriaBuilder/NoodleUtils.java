package paa.coder.noodleCriteriaBuilder;

import paa.coder.noodleCriteriaBuilder.exceptions.NoodleException;

import javax.persistence.Transient;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class NoodleUtils {


    public static <X> Function<Object[],X> defaultInitializer(Class<X> clazz){

        return (os) -> {
            for(Constructor<?> c : clazz.getConstructors()){
                if(c.getParameterTypes().length == os.length){
                    boolean isEqual = true;
                    int x = 0;
                    while(x < os.length || isEqual){
                        if(os[x] != null){
                            isEqual = c.getParameterTypes()[x].isAssignableFrom(os[x].getClass());
                        }
                        x++;
                    }
                    if(isEqual){
                        try{
                            return (X) c.newInstance(os);
                        }catch(InstantiationException | IllegalAccessException | InvocationTargetException e){
                            throw new NoodleException(String.format("error create %s(%s) : %s", clazz.getName(), os, e.getMessage()), e);
                        }
                    }
                }
            }
            throw new NoodleException(String.format("not found constructor %s(%s)", clazz.getName(), os));
        };

    }

    public static Path<?> pathFinder(Root<?> root, String field) throws NoodleException.FieldNotFound{
        try{
            return pathFinder(root, new StrShift(field));
        }catch(IllegalArgumentException e){
            throw new NoodleException.FieldNotFound(e.getMessage(), e, root.getJavaType(), field);
        }
    }

    public static  Path<?> pathFinder(Path<?> root, StrShift strShift){
        if(strShift.getLeft().isEmpty()){
            return root;
        }
        Path<?> path = root.get(strShift.getLeft().get());
        if(strShift.getRight().isPresent()){
            return pathFinder(path, new StrShift(strShift.getRight().get()));
        }

        return path;
    }

    public static class StrShift {
        private final String left;
        private final String right;

        public StrShift(String str){
            String[] items = str.split("\\.");
            if(items.length > 1){
                this.left = items[0];
                this.right = String.join(".",Arrays.copyOfRange(items, 1, items.length));
            }else{
                this.right = null;
                if(!items[0].isBlank()){
                    this.left = items[0];
                }else{
                    this.left = null;
                }
            }
        }

        public Optional<String> getLeft(){
            return Optional.ofNullable(left);
        }

        public Optional<String> getRight(){
            return Optional.ofNullable(right);
        }
    }

    public static Set<String> getEntityColumnNames(Class<?> clazz){
        return getEntityColumns(clazz).stream().map(Field::getName).collect(Collectors.toSet());

    }

    public static Set<Field> getEntityColumns(Class<?> clazz){
        Set<Field> fields = new HashSet<>();
        if(clazz.getSuperclass() != null){
            fields.addAll(getEntityColumns(clazz.getSuperclass()));
        }
        fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
        return fields
                .stream()
                .filter(f -> ignoreFieldAnnotation().stream().noneMatch(i -> Optional.ofNullable(f.getAnnotation(i)).isPresent()))
                .collect(Collectors.toSet());

    }

    public static Collection<Class<? extends Annotation>> ignoreFieldAnnotation(){return List.of(Transient.class);}
}
