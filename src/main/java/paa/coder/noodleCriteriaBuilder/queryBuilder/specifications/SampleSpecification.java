package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import com.fasterxml.jackson.core.JsonProcessingException;
import paa.coder.noodleCriteriaBuilder.NoodleFactory;
import paa.coder.noodleCriteriaBuilder.exceptions.ComparisonOperatorsNotSupport;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleExpression;
import paa.coder.noodleCriteriaBuilder.interfaces.PathFinder;
import paa.coder.noodleCriteriaBuilder.queryBuilder.expressions.SampleColumn;

import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.function.Supplier;

public class SampleSpecification extends NoodleSpecification{

    private final NoodleExpression expression;
    private final NoodleFactory noodleFactory;
    private final Object value;

    public SampleSpecification(NoodleFactory noodleFactory, String field, String operator, Object value, Boolean isNot){
        this(noodleFactory, SampleColumn.build(field, null), operator, value, isNot);
    }

    public SampleSpecification(NoodleFactory noodleFactory, NoodleExpression expression, String operator, Object value, Boolean isNot){
        super(operator,isNot);
        this.noodleFactory = noodleFactory;
        this.expression = expression;
        this.value = value;
    }


    private Predicate toStringPredicate(Expression<String> path,CriteriaBuilder b) throws JsonProcessingException{
        return toStringObject(path,getValue(path),b);
    }

    private <X extends Comparable<X>> Predicate toComparablePredicate(Expression<X> path,CriteriaBuilder b) throws JsonProcessingException{
        return toComparableObject(path,getValue(path),b);
    }

    private <X> Predicate toEqualsPredicate(Expression<X> path,CriteriaBuilder b) throws JsonProcessingException{
        return toEqualsObject(path,getValue(path),b);
    }

    private Predicate toPredicateInList(Expression<?> path, Collection<?> value, CriteriaBuilder b){
        if(value.isEmpty()){
            return b.isNull(path);
        }
        Supplier<Predicate> in = () -> path.in(value);
        switch(operator.toUpperCase().trim()){
            case "IN":{
                return in.get();
            }
            case "NOT IN":{
                return in.get().not();
            }
        }
        throw new ComparisonOperatorsNotSupport("operator for collection must be in ['IN' ,'NOT IN']");
    }

    private <T> T getValue(Expression<T> path) throws JsonProcessingException{
        return noodleFactory.getValue(path.getJavaType(), value);
    }

    @Override
    public Predicate apply(PathFinder var1, CommonAbstractCriteria var2, CriteriaBuilder criteriaBuilder){
        Expression<?> path = expression.apply(var1, var2, criteriaBuilder)
                .orElseThrow(()->new ComparisonOperatorsNotSupport(String.format("field %s not found ", expression)));

        Predicate predicate;
        if(value == null){
            predicate = toPredicateInNull(path, criteriaBuilder);
        }else{

            if(isIterable()){
                try{
                    predicate = toPredicateInList(path,
                                                  noodleFactory.getValueCollection(path.getJavaType(), value),
                                                  criteriaBuilder);
                }catch(JsonProcessingException e){
                    throw new ComparisonOperatorsNotSupport.List(String.format("error converting value %s from field %s to Collection<%s> : %s",
                                                                               value, expression,
                                                                               path.getJavaType().getName(),
                                                                               e.getMessage()), e);
                }
            }else{
                try{
                    if(String.class.isAssignableFrom(path.getJavaType())){
                        predicate = toStringPredicate((Expression<String>) path, criteriaBuilder);
                    }else if(Comparable.class.isAssignableFrom(path.getJavaType())){
                        predicate = toComparablePredicate((Expression<Comparable>)path, criteriaBuilder);
                    }else{
                        predicate = toEqualsPredicate(path,criteriaBuilder);
                    }

                }catch(JsonProcessingException e){
                    throw new ComparisonOperatorsNotSupport.Value(String.format("error converting value %s from field %s to %s : %s",
                                                                                value, expression,
                                                                                path.getJavaType().getName(),
                                                                                e.getMessage()), e);
                }
            }
        }

        return isNot ? predicate.not() : predicate;
    }
}
