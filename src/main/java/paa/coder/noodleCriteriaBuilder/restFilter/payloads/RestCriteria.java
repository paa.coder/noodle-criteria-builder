package paa.coder.noodleCriteriaBuilder.restFilter.payloads;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RestCriteria {
    private  String field;
    private  String operator = "=";
    private  Object value;
    private RestQuery query;

    public RestCriteria(String field, String operator, Object value){
        this.field = field;
        this.operator = operator;
        this.value = value;
    }

    public RestCriteria(RestQuery query){
        this.query = query;
    }
}
