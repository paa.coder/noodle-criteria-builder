package paa.coder.noodleCriteriaBuilder.queryBuilder.specifications;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.util.EntityManagerProvider;
import com.vladmihalcea.hibernate.type.util.SQLExtractor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;
import paa.coder.noodleCriteriaBuilder.DbContext;
import paa.coder.noodleCriteriaBuilder.app.models.Book;
import paa.coder.noodleCriteriaBuilder.interfaces.NoodleResult;
import paa.coder.noodleCriteriaBuilder.queryBuilder.MultiSelectQuery;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataSet(cleanBefore = true)
class SampleSpecificationTest extends DbContext {

    private final ConnectionHolder connectionHolder = () -> EntityManagerProvider.instance("sample").connection();

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toEquals(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("name", "The Man in the High Castle"));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name=?");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(1).first().hasFieldOrPropertyWithValue("id",2);
    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toNotEquals(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("name", "<>","The Man in the High Castle"));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name<>?");
        assertThat(where.stream().collect(Collectors.toList())).hasSize(2);

        MultiSelectQuery<Book> where1 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("name", "!=","The Man in the High Castle"));
        assertThat(SQLExtractor.from(where1.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name<>?");
        assertThat(where1.stream().collect(Collectors.toList())).hasSize(2);


    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toComparing(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("id", "<",2));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.id<2");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);

        MultiSelectQuery<Book> where2 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("id", "<=",2));
        assertThat(SQLExtractor.from(where2.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.id<=2");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where2::stream);

        MultiSelectQuery<Book> where3 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("id", ">=",2));
        assertThat(SQLExtractor.from(where3.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.id>=2");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where3::stream);

        MultiSelectQuery<Book> where4 = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("id", ">",2));
        assertThat(SQLExtractor.from(where4.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.id>2");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where4::stream);

    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toIn(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("name", "in", Set.of("Dune","Do Androids Dream of Electric Sheep?")));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name in (? , ?)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);

        MultiSelectQuery<Book> whereNot = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.whereNot("name", "in", Set.of("Dune","Do Androids Dream of Electric Sheep?")));
        assertThat(SQLExtractor.from(whereNot.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.name not in  (? , ?)");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) whereNot::stream);

    }

    @Test
    @DataSet(value = "sample/specificationTest.yml")
    void toNull(){

        MultiSelectQuery<Book> where = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("info.size", "is null",(Object) null));
        assertThat(SQLExtractor.from(where.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.size is null");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) where::stream);

        MultiSelectQuery<Book> whereNot = factory().select(Book.class).select(s -> s.select("id")).where(w -> w.where("info.size", "is not null",(Object)null));
        assertThat(SQLExtractor.from(whereNot.query())).isEqualTo("select book0_.id as col_0_0_ from book book0_ where book0_.size is not null");
        assertDoesNotThrow((ThrowingSupplier<Stream<NoodleResult>>) whereNot::stream);

    }

}