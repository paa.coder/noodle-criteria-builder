package paa.coder.noodleCriteriaBuilder.app.models;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.proxy.HibernateProxy;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "author")
@EqualsAndHashCode(exclude = "books")
@ToString(exclude = "books")
@NamedEntityGraph(name = "authorWithBooks",attributeNodes = {@NamedAttributeNode("books")})
public class Author {
    @Id
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "author",fetch = FetchType.LAZY)
    private Set<Book> books;


    public boolean isBooksProxy() {
        if (books instanceof HibernateProxy) {
            return true;
        }
        return false;
    }
}
