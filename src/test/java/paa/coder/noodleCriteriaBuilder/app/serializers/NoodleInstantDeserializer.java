package paa.coder.noodleCriteriaBuilder.app.serializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class NoodleInstantDeserializer extends StdDeserializer<Instant> {

    public NoodleInstantDeserializer() {
        this(null);
    }

    public NoodleInstantDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Instant deserialize(JsonParser jsonParser,
                               DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String dateString;
        ZoneId zone=getZone();
        try {
            if (node instanceof TextNode) {
                dateString = node.textValue();
            } else {
                dateString = node.get("date").asText();
                if (node.get("zone") != null) {
                    zone = ZoneId.of(node.get("zone").asText());
                }
            }
        }catch (Exception e){
            return null;
        }
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            .withZone(zone);
        return Instant.from(fmt.parse(dateString));
    }

    private ZoneId getZone(){
        return ZoneId.of("Europe/Moscow");
    }
}
