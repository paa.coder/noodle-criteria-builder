package paa.coder.noodleCriteriaBuilder.app.models;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@Entity
@Table(name = "book2")
public class Book2 {
    @Id
    private Integer id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author2 author;

    @Column(name = "author_id",updatable = false,insertable = false)
    private Integer authorId;

}
