package paa.coder.noodleCriteriaBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import paa.coder.noodleCriteriaBuilder.queryBuilder.DeleteQuery;
import paa.coder.noodleCriteriaBuilder.queryBuilder.SampleQuery;
import paa.coder.noodleCriteriaBuilder.queryBuilder.UpdateQuery;

import javax.persistence.Transient;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public interface NoodleFactory {

    SessionFactory sessionFactory();

    default Session getSession(){
        return sessionFactory().getCurrentSession();
    }

    default ObjectMapper getObjectMapper(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new Hibernate5Module());
        return mapper;
    }

    default <T> SampleQuery<T> select(Class<T> from){
        return new SampleQuery<>(from, this);
    }

    default <T> DeleteQuery<T> delete(Class<T> from){
        return new DeleteQuery<>(from, this);
    }

    default <T> UpdateQuery<T> update(Class<T> from){
        return new UpdateQuery<>(from, this);
    }

    default <T> void save(T entity){
        getSession().saveOrUpdate(entity);
    }

    default <T> T merge(T entity){
        return (T) getSession().merge(entity);
    }

    default <T> void delete(T entity){
        getSession().delete(entity);
    }

    default <T> T lazyLoad(Class<T> entity, Serializable id){
        return getSession().load(entity,id);

    }

    default <T> Optional<T> find(Class<T> entity, Serializable id){
        return Optional.ofNullable(getSession().get(entity,id));
    }



    default Collection<Class<? extends Annotation>> ignoreFieldAnnotation(){
        return NoodleUtils.ignoreFieldAnnotation();
    }

    default <T> T getValue(Class<T> clazz, Object value) throws JsonProcessingException{
        try{
            return getObjectMapper().convertValue(value, clazz);
        }catch(RuntimeException runtimeException){
            throw runtimeException;
        }
    }

    default <T> Collection<T> getValueCollection(Class<T> clazz, Object value) throws JsonProcessingException{
        if(value instanceof String){
            return getObjectMapper().readValue((String) value, getObjectMapper().getTypeFactory().constructCollectionType(Set.class, clazz));
        }else{
            return getObjectMapper().convertValue(value, getObjectMapper().getTypeFactory().constructCollectionType(Set.class, clazz));
        }

    }

    default Set<String> getEntityColumnNames(Class<?> clazz){
        return NoodleUtils.getEntityColumnNames(clazz);

    }

    default FetchStore fetchStore(){
        return FetchStore.Sample.SingletonWrapper.getInstance(this);
    }

    default Set<Field> getEntityColumns(Class<?> clazz){
        return NoodleUtils.getEntityColumns(clazz);

    }

}
